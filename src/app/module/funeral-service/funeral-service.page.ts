/* eslint-disable @typescript-eslint/naming-convention */
import { IAuthResponse } from 'src/app/core/models/login/auth-response.model';
import { Component, DoCheck, OnInit } from '@angular/core';
import { FuneralService } from 'src/app/core/service/funeral-service/funeral.service';
import { IPaginator } from 'src/app/core/models/paginator.model';
import { IFuneralService } from 'src/app/core/models/funeral-service/funeralService.model';
import { AddServiceComponent } from './components/add-service/add-service.component';
import { LoadingController, ModalController } from '@ionic/angular';
import { CookieService } from 'ngx-cookie-service';
import { AuthConstants } from 'src/app/core/config/auth-constants.config';
import { DataBaseService } from 'src/app/core/service/data-base/data-base.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OptionService } from 'src/app/core/service/option/option.service';
import { IOption } from 'src/app/core/models/option.model';
import { AlertService } from 'src/app/core/service/alert/alert.service';

@Component({
  selector: 'app-funeral-service',
  templateUrl: './funeral-service.page.html',
  styleUrls: ['./funeral-service.page.scss'],
})
export class FuneralServicePage implements OnInit, DoCheck {
  services: IFuneralService[];
  data: IAuthResponse;
  dataForm: FormGroup;
  constructor(
    private funeralService: FuneralService,
    private modalController: ModalController,
    private cookieService: CookieService,
    private dataBaseService: DataBaseService,
    private formBuilder: FormBuilder,
    private optionService: OptionService,
    private loadingController: LoadingController,
    private alertService: AlertService,
  ) {
    this.buildForm();
  }
  buildForm() {
    this.dataForm = this.formBuilder.group({
      fune_closed: ['', Validators.required],
      fune_cremated: ['', Validators.required],
      old_customer: ['', Validators.required],
      currency: ['', Validators.required],
    });
  }
  ngDoCheck(): void {
    this.data = JSON.parse(this.cookieService.get(AuthConstants.AUTH));
  }

  ngOnInit() {
    this.getAllFuneralServices();
    this.getOptions();
  }
  getOptions() {
    this.dataBaseService.getData(AuthConstants.OPTIONS)
    .subscribe(
      (res: IOption) => {
        this.dataForm.patchValue(res);
      }
    );
  }

  async getAllFuneralServices() {
    this.dataBaseService.existKey(AuthConstants.SERVICES).subscribe(
      (isExist: boolean) => {
        if(!(isExist)){
          this.funeralService.getAllFuneralService()
          .subscribe(
            (res: IPaginator<IFuneralService[]>) => {
              this.services = res.data;
              this.dataBaseService.storeAllData(AuthConstants.SERVICES,res.data);
            },
            (error: any) => {
              console.log(error);
            }
          );
        }else{
          this.dataBaseService.getData(AuthConstants.SERVICES).subscribe(
            (res: any) => {
              this.services = res;
            }
          );
        }
      }
    );
  }
  async addNewFuneralService() {
    const lastIndex = this.services.length - 1;
    const lastDiscount: IFuneralService = this.services[lastIndex];
    const modal = await this.modalController.create({
      component: AddServiceComponent,
      mode: 'ios',
      cssClass: 'modal-user-form',
      componentProps: {
        lastDiscountData: lastDiscount
      }
    });
    await modal.present();

    await modal.onWillDismiss()
    .then(result => {
      // this.getAllFuneralServices();
      this.update();
    });
  }

  update(){
    this.funeralService.getAllFuneralService()
    .subscribe(
      async (res: IPaginator<IFuneralService[]>) => {
        await this.dataBaseService.update(AuthConstants.SERVICES, res.data);
        await this.getAllFuneralServices();
      }
    );
  }

  async doRefresh(event) {
    this.update();
    await setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

  async saveChanges(): Promise<void> {
    const load = await this.loadingController.create({
      message: 'Guardando datos'
    });
    load.present();
    const dataFromForm = this.dataForm.value;
    const dataToSave: IOption = {
      fune_closed: dataFromForm.fune_closed,
      fune_cremated: dataFromForm.fune_cremated,
      old_customer: dataFromForm.old_customer,
      currency: dataFromForm.currency,
      id: 1
    };
    this.optionService.updateOption(dataToSave)
    .subscribe(
      (res: IOption) => {
        this.dataForm.patchValue(res);
        this.dataBaseService.storeOneData(AuthConstants.OPTIONS, res);
        load.dismiss();
      },
      (error: any) => {
        load.dismiss();
        this.alertService.presentAlert(JSON.stringify(error));
      }
    );
  }
}
