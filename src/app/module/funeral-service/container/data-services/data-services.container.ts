/* eslint-disable @angular-eslint/component-class-suffix */
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { AlertController, LoadingController, ModalController } from '@ionic/angular';
import { IFuneralService } from 'src/app/core/models/funeral-service/funeralService.model';
import { IServerResponseOnlyMessage } from 'src/app/core/models/server-response.model';
import { AlertService } from 'src/app/core/service/alert/alert.service';
import { ColorService } from 'src/app/core/service/color/color.service';
import { FuneralService } from 'src/app/core/service/funeral-service/funeral.service';
import { ToastService } from 'src/app/core/service/toast/toast.service';
import { EditServiceComponent } from '../../components/edit-service/edit-service.component';
@Component({
  selector: 'app-data-services',
  templateUrl: './data-services.container.html',
  styleUrls: ['./data-services.container.scss'],
})
export class DataServicesContainer implements OnInit {
  @Input() services: IFuneralService[];
  @Output() updateEventEmitter: EventEmitter<boolean> = new EventEmitter();
  displayedColumns: string[] = ['name', 'real_price', 'discount_price', 'predicted_price','action'];
  constructor(
    public colorService: ColorService,
    public modalController: ModalController,
    private alertController: AlertController,
    private funeralService: FuneralService,
    private loadingController: LoadingController,
    private alertService: AlertService,
    private toastService: ToastService
  ) { }

  ngOnInit() {}

  async editDiscount(data: IFuneralService) {
    const modal = await this.modalController.create({
      component: EditServiceComponent,
      mode: 'ios',
      cssClass: 'modal-user-form',
      componentProps: {
        discountData: data
      }
    });
    await modal.present();

    await modal.onWillDismiss()
    .then(result => {
      // this.getAllDiscounts();
      this.updateEventEmitter.emit(true);
    });
  }

  async deleteDiscount(data: IFuneralService){
    const alert = await this.alertController.create({
      header: `¿Eliminar el Servicio Funerario ${data.name} ?`,
      mode: 'ios',
      message: '<strong>Una vez eliminado no podrá recuperar los datos del Servicio Funerario seleccionado</strong>',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'btn-danger',
          id: 'cancel-button',
          handler: (blah) => {
            alert.dismiss();
          }
        }, {
          text: 'Eliminar',
          id: 'confirm-button',
          handler: () => {
            this.delete(data);
          }
        }
      ]
    });
    await alert.present();
  }

  async delete(data: IFuneralService) {
    const messageData = 'Eliminando datos, porfavor espera';
    const load = await this.loadingController.create({
      message: messageData
    });
    await load.present();
    this.funeralService.deleteFuneralService(data)
    .subscribe(
      (res: IServerResponseOnlyMessage) => {
        load.dismiss();
        const messageConfirm = `El descuento con el plazo en ${data.name} años, se eliminó correctamente`;
        this.toastService.presentToast(messageConfirm);

        this.updateEventEmitter.emit(true);
      },
      (error: any) => {
        load.dismiss();
        const headerData         = 'Ooops... ocurrió un problema';
        const subHeaderData      = 'Contactese con administración';
        const messageDataToAlert = JSON.stringify(error);
        this.alertService.presentAlert(headerData, subHeaderData, messageDataToAlert);
      }
    );
  }
}
