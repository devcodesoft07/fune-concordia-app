/* eslint-disable @typescript-eslint/naming-convention */
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { AuthConstants } from 'src/app/core/config/auth-constants.config';
import { IDiscount } from 'src/app/core/models/discount/discount-model';
import { IFuneralService } from 'src/app/core/models/funeral-service/funeralService.model';
import { ISector } from 'src/app/core/models/memorial-lot/sector.model';
import { IPaginator } from 'src/app/core/models/paginator.model';
import { IDataToQuote } from 'src/app/core/models/quotes/quote.model';
import { DataBaseService } from 'src/app/core/service/data-base/data-base.service';
import { DataServiceQuoteService } from 'src/app/core/service/data-service-cuote/data-service-quote.service';
import { DiscountService } from 'src/app/core/service/discount/discount.service';
import SwiperCore, { Autoplay, Navigation, Pagination, SwiperOptions } from 'swiper';
SwiperCore.use([Navigation, Pagination, Autoplay]);

@Component({
  selector: 'app-quote-service',
  templateUrl: './quote-service.component.html',
  styleUrls: ['./quote-service.component.scss'],
})
export class QuoteServiceComponent implements OnInit {
  @Input() services: IFuneralService[];
  serviceSelected!: IFuneralService;
  discountsList: IDiscount[];
  discount: IDiscount;
  quoteForm!: FormGroup;
  service: IFuneralService;
  config: SwiperOptions = {
    slidesPerView: 1,
    spaceBetween: 50,
    navigation: true,
    pagination: { clickable: true },
    scrollbar: { draggable: true },
    autoplay: true
  };
  constructor(
    private dataBaseService: DataBaseService,
    private formBuilder: FormBuilder,
    private navController: NavController,
    private dataServiceQuoteService: DataServiceQuoteService,
    private discountService: DiscountService,
  ) {
    this.buildForm();
  }

  buildForm(){
    this.quoteForm = this.formBuilder.group({
      data: ['', [ Validators.required]],
      discount: ['', [Validators.required]],
      old_customer: [false, [Validators.required]],
      closed_funeral: [false,[Validators.required]],
      crematorium_funeral: [false,[Validators.required]],
      title: ['Servicio Funerario']
    });
  }

  async ngOnInit() {
    const hasDiscountFune = this.dataBaseService.existKey(AuthConstants.DISCOUNTS_FUNE);
    if (hasDiscountFune) {
      this.dataBaseService.getData(AuthConstants.DISCOUNTS_FUNE).subscribe(
        async (res: any[]) => {
          this.discountsList = this.generateNewDiscountList(res);
          this.discount = await this.discountsList[0];
          this.service = await this.services[0];
          await this.quoteForm.patchValue({
            data: this.service,
            discount: this.discount
          });
        }
      );
    } else {
      this.getDiscountsFune();
    }
  }

  getDiscountsFune() {
    this.discountService.getAllDiscount(false).subscribe(
      (res: IPaginator<IDiscount[]>) => {
        this.dataBaseService.storeAllData(AuthConstants.DISCOUNTS_FUNE, res.data);
      },
      (error: any) => {
        this.dataBaseService.storeAllData(AuthConstants.DISCOUNTS_FUNE, []);
      }
    );
  }

  generateNewDiscountList(res: any[]): IDiscount[] {
    return res.filter(
      (discount: IDiscount) => discount.fune_value !== 1
    );
  }

  selectedService(event){
    this.serviceSelected = event.detail.value;
    this.service = event.detail.value;
  }

  async addNewQuote(){
    const dataToCuote: IDataToQuote<IFuneralService> = await this.quoteForm.value;
    const dataItem: IFuneralService = await this.quoteForm.controls.data.value;
    await this.dataBaseService.storeOneData(AuthConstants.DATATOCUOTE, dataToCuote);
    await this.dataServiceQuoteService.sendObjectSource(dataToCuote);
    await this.navController.navigateForward(`home/funeral-service-quote/${dataItem.id}`);
  }

}
