/* eslint-disable @typescript-eslint/naming-convention */
import { Component, Input, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingController, ModalController, ToastController } from '@ionic/angular';
import { IFuneralService, IFuneralServiceSave } from 'src/app/core/models/funeral-service/funeralService.model';
import { IImage } from 'src/app/core/models/memorial-lot/image.model';
import { AlertService } from 'src/app/core/service/alert/alert.service';
import { FuneralService } from 'src/app/core/service/funeral-service/funeral.service';
import { ToastService } from 'src/app/core/service/toast/toast.service';
import { FileUploadComponent } from 'src/app/shared/components/file-upload/file-upload.component';
@Component({
  selector: 'app-add-service',
  templateUrl: './add-service.component.html',
  styleUrls: ['./add-service.component.scss'],
})
export class AddServiceComponent implements OnInit {
  @Input() lastServiceData: IFuneralService;
  serviceForm: FormGroup;
  images: string[] = [];
  state: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private funeralService: FuneralService,
    private loadingController: LoadingController,
    private toastService: ToastService,
    public modalController: ModalController,
    private alertService: AlertService,
    private storage: AngularFireStorage
  ) {  }

  ngOnInit() {
    //this.serviceForm.controls.name.setValue(this.lastServiceData.name + 1);
    this.buildForm();
  }

  buildForm(): void {
    this.serviceForm = this.formBuilder.group({
      name: ['',[
        Validators.required
      ]],
      real_price: ['',[
        // Validators.required
      ]],
      discount_price: ['',[
        // Validators.required
      ]],
      predicted_price: ['',[
        Validators.required
      ]],
      image: [['empty', 'empty1']]
    });
  }

  closeModal(){
    this.modalController.dismiss();
  }

  async addNewService() {
    const predicted_price: number = this.serviceForm.value.predicted_price;
    const realPrice = Math.round(predicted_price*1.25);
    const discountPrice = realPrice - this.serviceForm.controls.predicted_price.value;
    this.serviceForm.controls.real_price.setValue(realPrice);
    this.serviceForm.controls.discount_price.setValue(discountPrice);
    const dataFromForm = this.serviceForm.value;
    const data: IFuneralServiceSave =  {
      name: dataFromForm.name,
      real_price: dataFromForm.real_price,
      discount_price: dataFromForm.discount_price,
      predicted_price: dataFromForm.predicted_price,
      image: this.generateImages(dataFromForm.image)
    };
    const messageData = 'Guardando datos, porfavor espera';
    const load = await this.loadingController.create({
      message: messageData
    });
    await load.present();
    this.funeralService.addFuneralService(data)
    .subscribe(
      (res: IFuneralService) => {
        const messageConfirm = `El nuevo descuento con el plazo en ${res.name} años, se agregó correctamente`;
        load.dismiss();
        this.toastService.presentToast(messageConfirm);
        this.modalController.dismiss();
      },
      (error: any) => {
        load.dismiss();
        const headerData         = 'Ooops... ocurrió un problema';
        const subHeaderData      = 'Contactese con administración';
        const messageDataToAlert = JSON.stringify(error);
        this.alertService.presentAlert(headerData, subHeaderData, messageDataToAlert);
      }
    );
  }

  generateImages(images: string[]): string[] {
    const firstImage: string = images[0];
    if (firstImage === 'empty') {
      return images;
    } else {
      const size: number = images.length;
      const count: number = 2 - size;
      if (size < 2) {
        for (let index = 0; index < count; index++) {
          images.push('empty' + index);
        }
      }
      return images;
    }
  }

  async fileUpload(){
    const modal = await this.modalController.create({
      component: FileUploadComponent,
      mode: 'ios',
      componentProps: {
        category: 'Services',
        userName: this.serviceForm.controls.name.value
      }

    });
    await modal.present();

    await modal.onDidDismiss()
    .then(
      (res: any) => {
        const voucher = res.data.voucher;
        if (voucher) {
          this.images.push(voucher);
          this.serviceForm.controls.image.setValue(this.images);
        }
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
      }
    );
  }


  deleteImage(image: string, index: number): void {
    this.state = true;
    this.storage.refFromURL(image).delete()
    .subscribe(
      (res: any) => {
        this.state = true;
        this.images.splice(index, 1);
        this.serviceForm.controls.image.setValue(this.images);
      }
    );
  }
}
