/* eslint-disable @typescript-eslint/naming-convention */
import { Component, Input, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingController, ModalController } from '@ionic/angular';
import { IFuneralService } from 'src/app/core/models/funeral-service/funeralService.model';
import { IImage } from 'src/app/core/models/memorial-lot/image.model';
import { IServerResponse } from 'src/app/core/models/server-response.model';
import { AlertService } from 'src/app/core/service/alert/alert.service';
import { FuneralService } from 'src/app/core/service/funeral-service/funeral.service';
import { ToastService } from 'src/app/core/service/toast/toast.service';
import { FileUploadComponent } from 'src/app/shared/components/file-upload/file-upload.component';

@Component({
  selector: 'app-edit-service',
  templateUrl: './edit-service.component.html',
  styleUrls: ['./edit-service.component.scss'],
})
export class EditServiceComponent implements OnInit {
  @Input() discountData: IFuneralService;
  serviceForm: FormGroup;
  imageForm: FormGroup;
  allImages!: IImage[];
  state: boolean;

  constructor(
    private modalController: ModalController,
    private formBuilder: FormBuilder,
    private funeralService: FuneralService,
    private loadingController: LoadingController,
    private toastService: ToastService,
    private alertService: AlertService,
    private storage: AngularFireStorage
  ) {
    this.buildForm();
  }

  get allImagesSector(){
    return (this.allImages && this.allImages[0]) ? this.allImages : null;
  }

  ngOnInit() {
    console.log(this.discountData);
    this.serviceForm.patchValue(this.discountData);
    this.getAllImagesOfSector();
  }

  closeModal(){
    this.modalController.dismiss();
  }

  getAllImagesOfSector(){
    this.funeralService.getAllImagesOfServices(this.discountData.id)
    .subscribe(
      (res: IServerResponse<IImage[]>) => {
        this.allImages = res.data;
        console.log('images', res.data);
      }
    );
  }

  buildForm(): void {
    this.serviceForm = this.formBuilder.group({
      id: ['',[]],
      name: ['',[
        Validators.required
      ]],
      real_price: ['',[
        // Validators.required
      ]],
      discount_price: ['',[
        // Validators.required
      ]],
      predicted_price: ['',[
        Validators.required
      ]],
      image: ['',[]]
    });

    this.imageForm = this.formBuilder.group({
      id: ['',[]],
      image: ['',[]]
    });
  }

  async editService(){
    const predicted_price: number = this.serviceForm.value.predicted_price;
    const realPrice = Math.round(predicted_price*1.25);
    const discountPrice = realPrice - this.serviceForm.controls.predicted_price.value;
    this.serviceForm.controls.real_price.setValue(realPrice);
    this.serviceForm.controls.discount_price.setValue(discountPrice);
    const data: IFuneralService = this.serviceForm.value;
    const messageData = 'Guardando datos, porfavor espera';
    const load = await this.loadingController.create({
      message: messageData
    });
    await load.present();
    this.funeralService.updateFuneralService(data)
    .subscribe(
      (res: IFuneralService) => {
        const messageConfirm = `Se actulizaron correctamente los datos del Servicio Funerario`;
        load.dismiss();
        this.toastService.presentToast(messageConfirm);
        this.closeModal();
      },
      (error: any) => {
        load.dismiss();
        const headerData = 'Ooops... ocurrió un problema';
        const subHeaderData = 'Contactese con administración';
        const messageDataToAlert = JSON.stringify(error);
        this.alertService.presentAlert(headerData, subHeaderData, messageDataToAlert);
      });
  }

  async fileUpload(){
    const image = this.getImageEmpty();
    const modal = await this.modalController.create({
      component: FileUploadComponent,
      mode: 'ios',
      componentProps: {
        category: 'Services',
        userName: this.serviceForm.controls.name.value
      }

    });
    await modal.present();

    await modal.onDidDismiss()
    .then(
      (res: any) => {
        const voucher = res.data.voucher;
        if (voucher) {
          this.imageForm.controls.id.setValue(image.id);
          this.imageForm.controls.image.setValue(voucher);
          const data = this.imageForm.value;
          this.funeralService.updateImageOfFuneralService(this.discountData.id, data)
          .subscribe(
            (response: IImage) =>{
              this.getAllImagesOfSector();
              console.log('UpdImag', response);
            }
          );
          console.log('voucher',voucher);
        }
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
      }
    );
  }

  deleteImage(image: IImage, index: number): void {
    this.state = true;
    const data: IImage = {
      id: image.id,
      image: 'empty' + index
    };
    this.funeralService.updateImageOfFuneralService(this.discountData.id, data)
    .subscribe(
      (response: IImage) =>{
        this.storage.refFromURL(image.image).delete()
        .subscribe(
          (res: any) => {
            this.state = false;
            this.getAllImagesOfSector();
          }
        );
      }
    );
  }

  getImageEmpty(): IImage {
    let image: IImage = {
      image: 'empty',
      id: 0
    };
    let isFoundImageEmpty = false;
    this.allImages.forEach(element => {
      if (element.image.includes('empty') || element.image === '') {
        if (!isFoundImageEmpty) {
          image = element;
          isFoundImageEmpty = true;
        }
      }
    });
    return image;
  }
}
