import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FuneralServicePageRoutingModule } from './funeral-service-routing.module';

import { IonicModule } from '@ionic/angular';

import { SharedModule } from 'src/app/shared/shared.module';

//pages
import { FuneralServicePage } from './funeral-service.page';

//Components and Container
import { DataServicesContainer } from './container/data-services/data-services.container';

//external module
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AngularMaterialModule } from 'src/app/shared/angular-material/angular-material.module';
import { AddServiceComponent } from './components/add-service/add-service.component';
import { EditServiceComponent } from './components/edit-service/edit-service.component';
import { AvatarModule } from 'ngx-avatar';
import { QuoteServiceComponent } from './components/quote-service/quote-service.component';
import { SwiperModule } from 'swiper/angular';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FuneralServicePageRoutingModule,
    SharedModule,
    NgxDatatableModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    AvatarModule,
    FormsModule,
    SwiperModule
  ],
  declarations: [
    FuneralServicePage,
    DataServicesContainer ,
    AddServiceComponent,
    EditServiceComponent,
    QuoteServiceComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FuneralServicePageModule {}
