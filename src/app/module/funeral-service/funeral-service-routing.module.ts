import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FuneralServicePage } from './funeral-service.page';

const routes: Routes = [
  {
    path: '',
    component: FuneralServicePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FuneralServicePageRoutingModule {}
