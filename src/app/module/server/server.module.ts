import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ServerPageRoutingModule } from './server-routing.module';

import { ServerPage } from './server.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ServerPageRoutingModule,
    SharedModule
  ],
  declarations: [ServerPage]
})
export class ServerPageModule {}
