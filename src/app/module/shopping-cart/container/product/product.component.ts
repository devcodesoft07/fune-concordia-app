import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { IFuneralService } from 'src/app/core/models/funeral-service/funeralService.model';
import { ISector } from 'src/app/core/models/memorial-lot/sector.model';
import { IDataToQuote } from 'src/app/core/models/quotes/quote.model';
import { DataServiceQuoteService } from 'src/app/core/service/data-service-cuote/data-service-quote.service';
import { ToastService } from 'src/app/core/service/toast/toast.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  @Input() item: IDataToQuote<ISector|IFuneralService>;
  @Output() deleteEventEmitter: EventEmitter<boolean> = new EventEmitter();
  @Output() cloneEventEmitter: EventEmitter<IDataToQuote<ISector|IFuneralService>> = new EventEmitter();
  constructor(
    private dataQuoteService: DataServiceQuoteService,
    private toastController: ToastService
  ) { }

  ngOnInit(): void {
  }
}
