import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ViewDidEnter } from '@ionic/angular';
import { DataServiceQuoteService } from 'src/app/core/service/data-service-cuote/data-service-quote.service';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.page.html',
  styleUrls: ['./shopping-cart.page.scss'],
})
export class ShoppingCartPage implements OnInit, ViewDidEnter {
  products: any;

  constructor(
    private dataSeriviceQuote: DataServiceQuoteService,
    private router: Router
  ) {
  }

  ionViewDidEnter(): void {
    this.dataSeriviceQuote.objectSourceList$
    .subscribe(
      (res: any) => {
        this.products = res;
        console.log(res);
      }
    )
    .unsubscribe();
  }

  ngOnInit() {
  }

  deleteProduct(index: number): void {
    this.dataSeriviceQuote.deleteProduct(index);
    this.ionViewDidEnter();
  }

  cloneProduct(data: any): void {
    this.dataSeriviceQuote.addProduct(data);
    this.ionViewDidEnter();
  }

  clearProduct(): void {
    this.dataSeriviceQuote.clearProducts();
    this.ionViewDidEnter();
  }

  sendToWhats(): void {
    const dataToSend = this.products[0];
    this.dataSeriviceQuote.sendObjectSource(dataToSend);
    this.router.navigate(['/home/view-cotization']);
  }
}
