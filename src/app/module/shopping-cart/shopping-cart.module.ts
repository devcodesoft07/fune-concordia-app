import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ShoppingCartPageRoutingModule } from './shopping-cart-routing.module';

import { ShoppingCartPage } from './shopping-cart.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProductComponent } from './container/product/product.component';
import { AngularMaterialModule } from 'src/app/shared/angular-material/angular-material.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShoppingCartPageRoutingModule,
    SharedModule,
    AngularMaterialModule
  ],
  declarations: [ShoppingCartPage, ProductComponent]
})
export class ShoppingCartPageModule {}
