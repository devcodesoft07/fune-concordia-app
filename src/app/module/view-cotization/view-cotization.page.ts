import { IFuneralService } from './../../core/models/funeral-service/funeralService.model';
/* eslint-disable max-len */
import { Component, OnInit } from '@angular/core';
import { AnimationOptions } from '@ionic/angular/providers/nav-controller';
import { AlertController, LoadingController, NavController, ViewDidEnter } from '@ionic/angular';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
import { ToastService } from 'src/app/core/service/toast/toast.service';
import { DataServiceQuoteService } from 'src/app/core/service/data-service-cuote/data-service-quote.service';
import { IDataToQuote, IQuotaServer } from 'src/app/core/models/quotes/quote.model';
import { GenerateQuotaService } from 'src/app/core/service/quota/generate-quota.service';
import { ISector } from 'src/app/core/models/memorial-lot/sector.model';
import { Router } from '@angular/router';
import { FormArray } from '@angular/forms';

@Component({
  selector: 'app-view-cotization',
  templateUrl: './view-cotization.page.html',
  styleUrls: ['./view-cotization.page.scss'],
})
export class ViewCotizationPage implements ViewDidEnter {
  loader: any = null;
  sharingText = 'Cotización Funeraria Khantutani';
  emailSubject = 'Download Apps';
  recipent = ['recipient@example.org'];
  sharingImage;
  sharingUrl = 'https://api-fune-app.herokuapp.com/quotePlace.pdf';
  constructor(
    private navCtrl: NavController,
    private socialSharing: SocialSharing,
    private toastService: ToastService,
    private loadController: LoadingController,
    private alertController: AlertController,
    private dataServiceQuoteService: DataServiceQuoteService,
    private quotaService: GenerateQuotaService,
    private router: Router
  ) { }

  ionViewDidEnter() {
    this.getAllData();
  }

  getAllData() {
    this.dataServiceQuoteService.objectSourceList$.subscribe(
      (res: IDataToQuote<ISector|IFuneralService> []) => {
        console.log(res);
        this.getQuotaData(res);
      }
    ).unsubscribe();
  }
  getQuotaData(data: IDataToQuote<ISector|IFuneralService>[]) {
    this.quotaService.getDataQuota(data)
    .subscribe(
      (res: any) => {
        this.sharingImage = res.data.url;
        this.shareVia();
        this.dataServiceQuoteService.clearProducts();
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  async shareVia() {
    const load = await this.loadController.create({
      message: 'Compartiendo...'
    });
    load.present();
    await this.socialSharing.shareVia('whatsapp', this.sharingText,'Fune Concordia' , this.sharingImage, '')
    .then((res) => {
      setTimeout(() => {
        load.dismiss();
      }, 1000);
      console.log(res);
      this.router.navigate(['/home']);
    })
    .catch((e) => {
      setTimeout(async () => {
        load.dismiss();
        const alert = await this.alertController.create({
          header: 'Error...',
          message: JSON.stringify(e)
        });
        alert.present();
        this.toastService.presentToast(JSON.stringify(e), 'danger');
      }, 1000);
      console.log('error', e);
    });
  }

  back(): void {
    const animations: AnimationOptions = {
      animated: true,
      animationDirection: 'back'
    };
    this.navCtrl.back(animations);
  }
}
