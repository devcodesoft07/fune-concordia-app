import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewCotizationPage } from './view-cotization.page';

const routes: Routes = [
  {
    path: '',
    component: ViewCotizationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewCotizationPageRoutingModule {}
