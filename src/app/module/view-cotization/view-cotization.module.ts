import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewCotizationPageRoutingModule } from './view-cotization-routing.module';

import { ViewCotizationPage } from './view-cotization.page';
import { AngularMaterialModule } from 'src/app/shared/angular-material/angular-material.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewCotizationPageRoutingModule,
    AngularMaterialModule
  ],
  declarations: [ViewCotizationPage]
})
export class ViewCotizationPageModule {}
