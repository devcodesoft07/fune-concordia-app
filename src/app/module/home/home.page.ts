import { Component, OnInit } from '@angular/core';
import { AuthConstants } from 'src/app/core/config/auth-constants.config';
import { IDiscount } from 'src/app/core/models/discount/discount-model';
import { IInitial } from 'src/app/core/models/initial.model';
import { IOption } from 'src/app/core/models/option.model';
import { IPaginator } from 'src/app/core/models/paginator.model';
import { IRoutes } from 'src/app/core/models/routes.model';
import { DataBaseService } from 'src/app/core/service/data-base/data-base.service';
import { DiscountService } from 'src/app/core/service/discount/discount.service';
import { InitialService } from 'src/app/core/service/initial/initial.service';
import { OptionService } from 'src/app/core/service/option/option.service';
import { RoutesService } from 'src/app/core/service/routes/routes.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  routes: IRoutes[] = [];
  constructor(
    private routesService: RoutesService,
    private discountService: DiscountService,
    private initialService: InitialService,
    private optionService: OptionService,
    private dataBaseService: DataBaseService,
  ) {
  }
  async ngOnInit() {
    this.getRoutes();
    this.dataBaseService.existKey(AuthConstants.DISCOUNTS).subscribe(
      (res: boolean) => {
        if (!(res)) {
          this.getDiscounts();
        }
      }
    );
    this.dataBaseService.existKey(AuthConstants.DISCOUNTS_FUNE).subscribe(
      (res: boolean) => {
        if (!(res)) {
          this.getDiscountsFune();
        }
      }
    );
    this.getInitials();
    this.getOptions();
  }
  getDiscountsFune() {
    this.discountService.getAllDiscount(false).subscribe(
      (res: IPaginator<IDiscount[]>) => {
        this.dataBaseService.storeAllData(AuthConstants.DISCOUNTS_FUNE, res.data);
      },
      (error: any) => {
        this.dataBaseService.storeAllData(AuthConstants.DISCOUNTS_FUNE, []);
      }
    );
  }
  getCurrency() {
  }
  getOptions() {
    this.optionService.getAllOptions()
    .subscribe(
      (res: IPaginator<IOption[]>) => {
        this.dataBaseService.storeOneData(AuthConstants.OPTIONS, res.data[0]);
      }
    );
  }
  getInitials() {
    this.initialService.getAllInitials()
    .subscribe(
      (res: IPaginator<IInitial[]>) => {
        this.dataBaseService.storeOneData(AuthConstants.INITIALS, res.data[0]);
      }
    );
  }

  getRoutes(): void {
    this.routesService.getRoutes().subscribe(
      (res: IRoutes[]) => {
        this.routes = res;
      },
      (error: any) => {
        console.log(error);
      }
    );
  }
  getDiscounts(): void {
    this.discountService.getAllDiscount().subscribe(
      (res: IPaginator<IDiscount[]>) => {
        this.dataBaseService.storeAllData(AuthConstants.DISCOUNTS, res.data);
      },
      (error: any) => {
        console.log(error);
      }
    );

  }

}
