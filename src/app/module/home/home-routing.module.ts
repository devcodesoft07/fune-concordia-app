import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: '',
    component: HomePage,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'memorial-lot' },
      {
        path: 'memorial-lot',
        loadChildren: () => import('../memorial-lot/memorial-lot.module').then( m => m.MemorialLotPageModule)
      },
      {
        path: 'funeral-service',
        loadChildren: () => import('../funeral-service/funeral-service.module').then( m => m.FuneralServicePageModule)
      },
      {
        path: 'shopping-cart',
        loadChildren: () => import('../shopping-cart/shopping-cart.module').then( m => m.ShoppingCartPageModule)
      },
      {
        path: 'discount',
        loadChildren: () => import('../discount/discount.module').then( m => m.DiscountPageModule)
      },
      {
        path: 'user',
        loadChildren: () => import('../user/user.module').then( m => m.UserPageModule)
      },
      {
        path: 'server',
        loadChildren: () => import('../server/server.module').then( m => m.ServerPageModule)
      },
      // {
      //   path: 'sector/:id',
      //loadChildren: () => import('../memorial-lot/detail-memorial-lot/detail-memorial-lot.module').then(m=> m.DetailMemorialLotPageModule)
      // }
      {
        path: 'funeral-service-quote/:id',
        loadChildren: () => import('../funeral-service-quote/funeral-service-quote.module').then( m => m.FuneralServiceQuotePageModule)
      },
      {
        path: 'view-cotization',
        loadChildren: () => import('../view-cotization/view-cotization.module').then( m => m.ViewCotizationPageModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomePageRoutingModule {}
