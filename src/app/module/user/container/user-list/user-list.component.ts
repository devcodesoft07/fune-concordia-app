import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { UsersService } from './../../../../core/service/users/users.service';
import { User } from 'src/app/core/models/user/user.model';
import { EditUserComponent } from '../../components/edit-user/edit-user.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent implements OnInit {
  @Output() reloadEventEmitter: EventEmitter<any> = new EventEmitter();
  @Input() userList: User[] | null | undefined;
  isActive = true;

  constructor(
    private modalController: ModalController,
    private usersService: UsersService,
    private alertController: AlertController
  ) { }

  ngOnInit() {}

  async updateUser(user: User) {
    const modal = await this.modalController.create({
      component: EditUserComponent,
      cssClass: 'modal-form',
      mode: 'ios',
      componentProps: {
        userData: user
      }
    });
    await modal.present();
    await modal.onDidDismiss()
    .then(
      (res: any) => {
        const reload = res.data.save;
        if (reload) {
          this.reloadEventEmitter.emit(true);
        }
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
      }
    );
  }

  async deleteUser(data: User){
    const alert = await this.alertController.create({
      header: `¿Eliminar al usuario ${data.name} ?`,
      mode: 'ios',
      message: '<strong>Una vez eliminado no podrá recuperar los datos del usuario</strong>',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'btn-danger',
          id: 'cancel-button',
          handler: (blah) => {
            alert.dismiss();
          }
        }, {
          text: 'Eliminar',
          id: 'confirm-button',
          handler: () => {
            this.delete(data);
          }
        }
      ]
    });
    await alert.present();
  }

  delete(user: User){
    this.usersService.deleteUser(user.id)
    .subscribe(
      (res: any) => {
        this.reloadEventEmitter.emit(true);
      }
    );
  }
}
