import { UserListComponent } from './container/user-list/user-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import { UserPageRoutingModule } from './user-routing.module';

import { UserPage } from './user.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { CreateUserComponent } from './components/create-user/create-user.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { EditUserComponent } from './components/edit-user/edit-user.component';
import { AvatarModule } from 'ngx-avatar';
import { AngularMaterialModule } from 'src/app/shared/angular-material/angular-material.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UserPageRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    AvatarModule,
    AngularMaterialModule
  ],
  declarations: [
    UserPage,
    UserListComponent,
    CreateUserComponent,
    EditUserComponent
  ]
})
export class UserPageModule {}
