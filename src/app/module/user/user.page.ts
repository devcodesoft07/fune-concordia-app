import { User } from './../../core/models/user/user.model';
import { UsersService } from './../../core/service/users/users.service';
import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { IonInfiniteScroll, ModalController, SearchbarCustomEvent, ViewDidEnter, ViewDidLeave } from '@ionic/angular';
import { CreateUserComponent } from './components/create-user/create-user.component';
import { IPaginator, ITotalUsers } from 'src/app/core/models/paginator.model';
import { CookieService } from 'ngx-cookie-service';
import { AuthConstants } from 'src/app/core/config/auth-constants.config';
import { UsersGetService } from 'src/app/core/service/users/users-get.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-user',
  templateUrl: './user.page.html',
  styleUrls: ['./user.page.scss'],
})
export class UserPage implements OnInit, ViewDidEnter, OnDestroy, ViewDidLeave {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  userList: User[];
  totalUsers: ITotalUsers | undefined | null;
  rol: string;
  data: any;
  users$: Observable<User[]>;
  paginator: IPaginator<User[]>;
  currenPage: number;
  textToSearch = '';

  constructor(
    private modalController: ModalController,
    private usersService: UsersService,
    private cookieService: CookieService,
    private usersGetService: UsersGetService,
    private cdr: ChangeDetectorRef
  ) {
    this.users$ = this.usersGetService.usersObserver$;
  }

  ionViewDidLeave(): void {
    this.usersGetService.clearDataUsers();
  }

  ngOnDestroy(): void {
    this.usersGetService.clearDataUsers();
  }
  ionViewDidEnter(): void {
    this.data = JSON.parse(this.cookieService.get(AuthConstants.AUTH));
    this.rol = this.data.user.role_name;
    if (this.rol === 'Administrador') {
      this.getAllUsers();
    }
  }

  ngOnInit() {
    this.data = JSON.parse(this.cookieService.get(AuthConstants.AUTH));
    this.rol = this.data.user.role_name;
  }

  async addUser( option: string) {
    const modal = await this.modalController.create({
      component: CreateUserComponent,
      mode: 'ios',
      cssClass: 'modal-form',
      componentProps: {
        opt: option,
      }
    });
    await modal.present();

    await modal.onDidDismiss()
    .then(
      (res: any) => {
        const reload = res.data.save;
        if (reload) {
          this.getAllUsersAfterEditOrDelete();
        }
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
      }
    );
  }

  getAllUsers(page: number = 1){
    this.usersService.getAllUsers(page)
    .subscribe(
      (res: IPaginator<User[]>) => {
        if (res.data.length !== 0) {
          this.paginator = res;
          this.currenPage = res.meta.current_page;
          this.userList = res.data;
          this.usersGetService.addUsers(res.data);
          this.totalUsers = res.users;
        } else {
          this.currenPage = res.meta.last_page;
        }
        this.cdr.detectChanges();
      }
    );
  }

  loadData(event) {
    if (this.rol === 'Administrador') {
      setTimeout(() => {
        this.getAllUsers(this.currenPage + 1);
        event.target.complete();
      }, 500);
    }
  }

  async doRefresh(event) {
    this.getAllUsersAfterEditOrDelete();
    await setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

  getAllUsersAfterEditOrDelete(): void {
    this.usersGetService.clearDataUsers();
    this.getAllUsers();
  }

  onSearchChange(event: any): void {
    const dataFormSearchbar: SearchbarCustomEvent = event;
    this.textToSearch = dataFormSearchbar.detail.value;
  }
}
