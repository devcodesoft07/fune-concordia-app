/* eslint-disable no-underscore-dangle */
/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable @typescript-eslint/naming-convention */
import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { LoadingController, ModalController } from '@ionic/angular';
import { User } from 'src/app/core/models/user/user.model';
import { UsersService } from './../../../../core/service/users/users.service';
import { PatternEnum } from 'src/app/core/enum/pattern-enum';
import { FileUploadComponent } from 'src/app/shared/components/file-upload/file-upload.component';
import Swal from 'sweetalert2';
import { ToastService } from 'src/app/core/service/toast/toast.service';
import { AlertService } from 'src/app/core/service/alert/alert.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss'],
})
export class EditUserComponent implements OnInit {
  @Input() userData: User;
  updateUserForm: FormGroup;
  statusUser: boolean;

  constructor(
    private modalController: ModalController,
    private usersService: UsersService,
    private formBuilder: FormBuilder,
    private loadingController: LoadingController,
    private toastService: ToastService,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.buildForm();
    this._getUserById();
    console.log('UserData', this.userData);
    this.updateUserForm.patchValue(this.userData);
    // console.log('FormPatch', this.updateUserForm.value);
  }

  private _getUserById(): void {
    this.usersService.getAllUserById(this.userData.id)
    .subscribe(
      (res: User) => {
        console.log(res);
      }
    );
  }

  get userNameInput(){
    return this.updateUserForm.get('name');
  }

  get userEmail(){
    return this.updateUserForm.get('email');
  }

  get userPassword(){
    return this.updateUserForm.get('password');
  }

  change($event: any){
    console.log('event', $event.detail.checked);
    this.statusUser = $event.detail.checked;
  }

  closeModal() {
    this.modalController.dismiss({
      save: false
    });
  }

  buildForm(){
    this.updateUserForm = this.formBuilder.group({
      id: [''],
      name: ['',[
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(255),
        Validators.pattern(PatternEnum.PATTERN_ONLY_TEXT)
      ]],
      email: ['', [
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(50),
        Validators.pattern(PatternEnum.EMAIL_PATTERN),
      ]],
      photo: ['', []],
      phone: ['', []],
      role_id: ['', [
        Validators.required,
        Validators.pattern('[0-9]')
      ]],
      status: ['',[
        Validators.pattern('[A-Z]')
      ]]
    });
  }

  async updateUser(){
    if(this.statusUser){
      this.updateUserForm.controls.status.setValue('ACTIVE');
    }else{
      this.updateUserForm.controls.status.setValue('INACTIVE');
    }
    const data = this.updateUserForm.value;
    console.log('FORM-UPDATA', this.updateUserForm.value);
    const messageData = 'Guardando datos, porfavor espera';
    const load = await this.loadingController.create({
      message: messageData
    });
    await load.present();
    this.usersService.updateUser(data)
    .subscribe(
      (res: User) => {
        const messageConfirm = `Los datos del usuario se actualizaron correctamente`;
        load.dismiss();
        this.toastService.presentToast(messageConfirm);
        this.modalController.dismiss({
          save: true
        });
        console.log('Up-Server', res);
      },
      (error: any) => {
        load.dismiss();
        const headerData = 'Ooops... ocurrió un problema';
        const subHeaderData = 'Contactese con administración';
        const messageDataToAlert = JSON.stringify(error);
        this.alertService.presentAlert(headerData, subHeaderData, messageDataToAlert);
        this.modalController.dismiss({
          save: false
        });
        console.log(error);
    });
  }

  async fileUpload() {
    const modal = await this.modalController.create({
      component: FileUploadComponent,
      mode: 'ios',
      componentProps: {
        category: 'user-photos',
        userName: this.updateUserForm.controls.name.value
      }
    });
    await modal.present();

    await modal.onDidDismiss()
    .then(
      (res: any) => {
        const voucher = res.data.voucher;
        if (voucher) {
          this.updateUserForm.controls.photo.setValue(voucher);
        }
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
      }
    );
  }
}
