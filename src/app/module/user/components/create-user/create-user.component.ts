/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable @typescript-eslint/naming-convention */
import { ToastService } from 'src/app/core/service/toast/toast.service';
import { UsersService } from '../../../../core/service/users/users.service';
import { LoadingController, ModalController } from '@ionic/angular';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PatternEnum } from 'src/app/core/enum/pattern-enum';
import { User } from 'src/app/core/models/user/user.model';
import { FileUploadComponent } from 'src/app/shared/components/file-upload/file-upload.component';
import Swal from 'sweetalert2';
import { AlertService } from 'src/app/core/service/alert/alert.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss'],
})
export class CreateUserComponent implements OnInit {
  @Output() reloadEventEmitter: EventEmitter<any> = new EventEmitter();
  userForm: FormGroup;
  userName: string;
  statusUser: boolean;

  constructor(
    private modalController: ModalController,
    private formBuilder: FormBuilder,
    private usersService: UsersService,
    private toastService: ToastService,
    private loadingController: LoadingController,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.buildForm();
  }

  get userNameInput(){
    return this.userForm.get('name');
  }

  get userEmail(){
    return this.userForm.get('email');
  }

  get userPassword(){
    return this.userForm.get('password');
  }

  buildForm(){
    this.userForm = this.formBuilder.group({
      name: ['',[
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(255),
        Validators.pattern(PatternEnum.PATTERN_ONLY_TEXT)
      ]],
      email: ['', [
        Validators.required,
        Validators.minLength(15),
        Validators.maxLength(50),
        Validators.pattern(PatternEnum.EMAIL_PATTERN),

      ]],
      password: ['',[
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(15)
        // Validators.pattern(PatternEnum.PATTERN_PASSWORD)
      ]],
      photo: ['', [

      ]],
      phone: ['', [

      ]],
      role_id: ['', [
        Validators.required,
        Validators.pattern('[0-9]')
      ]],
      status: ['',[
        // Validators.required,
        Validators.pattern('A-Z')
      ]]
    });
  }

  change($event: any){
    console.log('event', $event.detail.checked);
    this.statusUser = $event.detail.checked;
  }

  closeModal() {
    this.modalController.dismiss({
      save: false
    });
  }

  async createUser(){
    if(this.statusUser){
      this.userForm.controls.status.setValue('ACTIVE');
    }else{
      this.userForm.controls.status.setValue('INACTIVE');
    }
    const messageData = 'Guardando datos, porfavor espera';
    const load = await this.loadingController.create({
      message: messageData
    });
    await load.present();
    console.log('FormSend', this.userForm.value);
    this.usersService.createUser(this.userForm.value)
    .subscribe(
      (res: User) => {
        this.reloadEventEmitter.emit(true);
        const messageConfirm = `El nuevo usuario, se agregó correctamente`;
        load.dismiss();
        this.toastService.presentToast(messageConfirm);
        this.modalController.dismiss();
        this.modalController.dismiss({
          save: true
        });
        console.log('USUARIO CREADO', res);
      },
      (error: any) => {
        load.dismiss();
        console.log(error);
        const headerData = 'Ooops... ocurrió un problema';
        const subHeaderData = 'Contactese con administración';
        const messageDataToAlert = JSON.stringify(error);
        this.alertService.presentAlert(headerData, subHeaderData, messageDataToAlert);
        this.modalController.dismiss({
          save: false
        });
      }
    );
  }

  async fileUpload() {
    const modal = await this.modalController.create({
      component: FileUploadComponent,
      mode: 'ios',
      componentProps: {
        category: 'user-photos',
        userName: this.userName
      }

    });
    await modal.present();

    await modal.onDidDismiss()
    .then(
      (res: any) => {
        const voucher = res.data.voucher;
        if (voucher) {
          this.userForm.controls.photo.setValue(voucher);
        }
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
      }
    );
  }

}
