/* eslint-disable @angular-eslint/component-class-suffix */
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { AlertController, LoadingController, ModalController } from '@ionic/angular';
import { CookieService } from 'ngx-cookie-service';
import { AuthConstants } from 'src/app/core/config/auth-constants.config';
import { IDiscount } from 'src/app/core/models/discount/discount-model';
import { IInitial } from 'src/app/core/models/initial.model';
import { IAuthResponse } from 'src/app/core/models/login/auth-response.model';
import { IServerResponseOnlyMessage } from 'src/app/core/models/server-response.model';
import { AlertService } from 'src/app/core/service/alert/alert.service';
import { ColorService } from 'src/app/core/service/color/color.service';
import { DiscountService } from 'src/app/core/service/discount/discount.service';
import { ToastService } from 'src/app/core/service/toast/toast.service';
import { EditDiscountFuneComponent } from '../../components/edit-discount-fune/edit-discount-fune.component';
import { EditDiscountComponent } from '../../components/edit-discount/edit-discount.component';

@Component({
  selector: 'app-data-discounts',
  templateUrl: './data-discounts.container.html',
  styleUrls: ['./data-discounts.container.scss']
})
export class DataDiscountsContainer implements OnInit {

  @Input() discounts: IDiscount[];
  @Input() initials: IInitial;
  @Input() isFune = false;
  @Output() updateEventEmitter: EventEmitter<boolean> = new EventEmitter();
  displayedColumns: string[];
  data: IAuthResponse;
  constructor(
    public colorService: ColorService,
    public modalController: ModalController,
    private alertController: AlertController,
    private discountService: DiscountService,
    private loadingController: LoadingController,
    private alertService: AlertService,
    private toastService: ToastService,
    private cookieService: CookieService
  ) {
  }

  ngOnInit(): void {
    this.data = JSON.parse(this.cookieService.get(AuthConstants.AUTH));
    this.buildColumns();
  }

  buildColumns(): void {
    if (this.isFune) {
      this.displayedColumns = ['deadline','funeral', 'quota-6', 'quota-10', 'quota-20', 'cash', 'action'];
    } else {
      this.displayedColumns = ['deadline', 'lot', 'quota-6', 'quota-10', 'quota-20', 'cash', 'action'];
    }
  }

  async editDiscount(data: IDiscount) {
    const modal = await this.modalController.create({
      component: EditDiscountComponent,
      mode: 'ios',
      cssClass: 'modal-form',
      componentProps: {
        discountData: data,
        initial: this.initials
      }
    });
    const modalFune = await this.modalController.create({
      component: EditDiscountFuneComponent,
      mode: 'ios',
      cssClass: 'modal-form',
      componentProps: {
        discountData: data,
        initial: this.initials
      }
    });

    if (this.isFune) {
      await modalFune.present();
    } else {

      await modal.present();
    }


    await modal.onWillDismiss()
    .then(result => {
      // this.getAllDiscounts();
      this.updateEventEmitter.emit(true);
    });
    await modalFune.onWillDismiss()
    .then(result => {
      // this.getAllDiscounts();
      this.updateEventEmitter.emit(true);
    });
  }

  async deleteDiscount(data: IDiscount){
    const alert = await this.alertController.create({
      header: `¿Eliminar descuento en ${data.term} años?`,
      mode: 'ios',
      message: '<strong>Una vez eliminado no podrá recuperar los datos del descuento seleccionado</strong>',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'btn-danger',
          id: 'cancel-button',
          handler: (blah) => {
            alert.dismiss();
          }
        }, {
          text: 'Eliminar',
          id: 'confirm-button',
          handler: () => {
            this.delete(data);
          }
        }
      ]
    });
    await alert.present();
  }
  async delete(data: IDiscount) {
    const messageData = 'Eliminando datos, porfavor espera';
    const load = await this.loadingController.create({
      message: messageData
    });
    await load.present();
    if (this.isFune) {
      this.discountService.deleteDiscount(data, false)
      .subscribe(
        (res: IServerResponseOnlyMessage) => {
          load.dismiss();
          const messageConfirm = `El descuento con el plazo en ${data.term} años, se eliminó correctamente`;
          this.toastService.presentToast(messageConfirm);
          this.updateEventEmitter.emit(true);
        },
        (error: any) => {
          load.dismiss();
          const headerData         = 'Ooops... ocurrió un problema';
          const subHeaderData      = 'Contactese con administración';
          const messageDataToAlert = JSON.stringify(error);
          this.alertService.presentAlert(headerData, subHeaderData, messageDataToAlert);
        }
      );
    } else {
      this.discountService.deleteDiscount(data)
      .subscribe(
        (res: IServerResponseOnlyMessage) => {
          load.dismiss();
          const messageConfirm = `El descuento con el plazo en ${data.term} años, se eliminó correctamente`;
          this.toastService.presentToast(messageConfirm);
          this.updateEventEmitter.emit(true);
        },
        (error: any) => {
          load.dismiss();
          const headerData         = 'Ooops... ocurrió un problema';
          const subHeaderData      = 'Contactese con administración';
          const messageDataToAlert = JSON.stringify(error);
          this.alertService.presentAlert(headerData, subHeaderData, messageDataToAlert);
        }
      );
    }

  }
}
