import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataDiscountsContainer } from './data-discounts.container';

describe('DataDiscountsContainer', () => {
  let component: DataDiscountsContainer;
  let fixture: ComponentFixture<DataDiscountsContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataDiscountsContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataDiscountsContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
