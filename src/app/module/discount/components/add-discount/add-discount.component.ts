/* eslint-disable @typescript-eslint/naming-convention */
import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingController, ModalController, ToastController } from '@ionic/angular';
import { IDiscount } from 'src/app/core/models/discount/discount-model';
import { IInitial } from 'src/app/core/models/initial.model';
import { AlertService } from 'src/app/core/service/alert/alert.service';
import { DiscountService } from 'src/app/core/service/discount/discount.service';
import { ToastService } from 'src/app/core/service/toast/toast.service';
@Component({
  selector: 'app-add-discount',
  templateUrl: './add-discount.component.html',
  styleUrls: ['./add-discount.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddDiscountComponent implements OnInit {
  @Input() lastDiscountData: IDiscount;
  @Input() initial: IInitial;
  discountForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private discountService: DiscountService,
    private loadingController: LoadingController,
    private toastService: ToastService,
    public modalController: ModalController,
    private alertService: AlertService
  ) {
    this.buildForm();
  }

  buildForm(): void {
    this.discountForm = this.formBuilder.group({
      term: [ ,
        [
          Validators.required
        ]
      ],
      lot_percentage: [ ,
        [
          Validators.required
        ]
      ],
      lot_value: [ ,
        [
          Validators.required
        ]
      ],
      fune_percentage: [ 0,
        [
          Validators.required
        ]
      ],
      fune_value: [ 0,
        [
          Validators.required
        ]
      ],
      initial_6: [ ,
        [
          Validators.required
        ]
      ],
      initial_10: [ ,
        [
          Validators.required
        ]
      ],
      initial_20: [ ,
        [
          Validators.required
        ]
      ],
      cash: [ 10,
        [
          Validators.required
        ]
      ]
    });
  }

  ngOnInit() {
    this.discountForm.controls.term.setValue(this.lastDiscountData.term + 1);
  }

  async addNewDiscount() {
    const value: IDiscount = this.discountForm.value;
    const messageData = 'Guardando datos, porfavor espera';
    const load = await this.loadingController.create({
      message: messageData
    });
    await load.present();
    this.discountService.addDiscount(value)
    .subscribe(
      (res: IDiscount) => {
        const messageConfirm = `El nuevo descuento con el plazo en ${res.term} años, se agregó correctamente`;
        load.dismiss();
        this.toastService.presentToast(messageConfirm);
        this.modalController.dismiss();
        // Swal.fire({
        //   icon: 'success',
        //   title: 'Datos agregados correctamente',
        //   text: `El nuevo descuento con el plazo en ${res.term} años, se agregó correctamente`,
        //   showConfirmButton: true,
        //   // timer: 2000,
        //   backdrop: true,
        //   background: 'transparent'
        // });
      },
      (error: any) => {
        load.dismiss();
        const headerData         = 'Ooops... ocurrió un problema';
        const subHeaderData      = 'Contactese con administración';
        const messageDataToAlert = JSON.stringify(error);
        this.alertService.presentAlert(headerData, subHeaderData, messageDataToAlert);
      }
    );
  }
}
