import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDiscountFuneComponent } from './edit-discount-fune.component';

describe('EditDiscountFuneComponent', () => {
  let component: EditDiscountFuneComponent;
  let fixture: ComponentFixture<EditDiscountFuneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditDiscountFuneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDiscountFuneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
