import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDiscountFuneComponent } from './add-discount-fune.component';

describe('AddDiscountFuneComponent', () => {
  let component: AddDiscountFuneComponent;
  let fixture: ComponentFixture<AddDiscountFuneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddDiscountFuneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDiscountFuneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
