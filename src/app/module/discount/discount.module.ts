import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DiscountPageRoutingModule } from './discount-routing.module';

import { IonicModule } from '@ionic/angular';

import { SharedModule } from 'src/app/shared/shared.module';

//pages
import { DiscountPage } from './discount.page';

//Components and Container
import { DataDiscountsContainer } from './container/data-discounts/data-discounts.container';

//external module
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AngularMaterialModule } from 'src/app/shared/angular-material/angular-material.module';
import { AddDiscountComponent } from './components/add-discount/add-discount.component';
import { EditDiscountComponent } from './components/edit-discount/edit-discount.component';
import { AddDiscountFuneComponent } from './components/add-discount-fune/add-discount-fune.component';
import { EditDiscountFuneComponent } from './components/edit-discount-fune/edit-discount-fune.component';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiscountPageRoutingModule,
    SharedModule,
    NgxDatatableModule,
    AngularMaterialModule,
    ReactiveFormsModule
  ],
  declarations: [
    DiscountPage,
    DataDiscountsContainer,
    AddDiscountComponent,
    EditDiscountComponent,
    AddDiscountFuneComponent,
    EditDiscountFuneComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DiscountPageModule {}
