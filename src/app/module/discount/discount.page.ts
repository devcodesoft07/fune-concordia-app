import { ChangeDetectionStrategy, Component, DoCheck, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { LoadingController, ModalController } from '@ionic/angular';
import { CookieService } from 'ngx-cookie-service';
import { AuthConstants } from 'src/app/core/config/auth-constants.config';
import { IDiscount } from 'src/app/core/models/discount/discount-model';
import { IInitial } from 'src/app/core/models/initial.model';
import { IAuthResponse } from 'src/app/core/models/login/auth-response.model';
import { IPaginator } from 'src/app/core/models/paginator.model';
import { AlertService } from 'src/app/core/service/alert/alert.service';
import { DataBaseService } from 'src/app/core/service/data-base/data-base.service';
import { DiscountService } from 'src/app/core/service/discount/discount.service';
import { InitialService } from 'src/app/core/service/initial/initial.service';
import { AddDiscountFuneComponent } from './components/add-discount-fune/add-discount-fune.component';
import { AddDiscountComponent } from './components/add-discount/add-discount.component';
import { EditDiscountComponent } from './components/edit-discount/edit-discount.component';
export interface Data {
  movies: string;
}
@Component({
  selector: 'app-discount',
  templateUrl: './discount.page.html',
  styleUrls: ['./discount.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DiscountPage implements OnInit, DoCheck {
  discounts: IDiscount[];
  data: IAuthResponse;
  initialData: IInitial;
  dataForm: FormGroup;
  discountsFune: IDiscount[] = [];
  isFune = false;


  constructor(
    private discountService: DiscountService,
    private dialogController: MatDialog,
    private modalController: ModalController,
    private dataBaseService: DataBaseService,
    private cookieService: CookieService,
    private formBuilder: FormBuilder,
    private initialService: InitialService,
    private loadingController: LoadingController,
    private alertService: AlertService
  ) {
    this.buildForm();
  }
  buildForm() {
    this.dataForm = this.formBuilder.group({
      first: ['', Validators.required],
      second: ['', Validators.required],
      third: ['', Validators.required],
    });
  }

  ngDoCheck(): void {
    this.data = JSON.parse(this.cookieService.get(AuthConstants.AUTH));
  }

  ngOnInit() {
    this.getAllDiscounts();
    this.getInitialData();
  }
  getInitialData() {
    this.dataBaseService.getData(AuthConstants.INITIALS).subscribe(
      (res: any) => {
        this.initialData = res;
        this.dataForm.patchValue(res);
      }
    );
  }

  getAllDiscounts() {
    this.dataBaseService.getData(AuthConstants.DISCOUNTS).subscribe(
      (res: any) => {
        this.discounts = res;
      }
    );
    this.dataBaseService.getData(AuthConstants.DISCOUNTS_FUNE).subscribe(
      (res: any) => {
        this.discountsFune = res;
      }
    );
  }

  async addNewDiscount() {
    const lastIndex = this.discounts.length - 1;
    const lastIndexFune = this.discountsFune? this.discountsFune.length - 1 : 0;
    const lastDiscount: IDiscount = this.discounts[lastIndex];
    const lastDiscountFune: IDiscount = this.discounts[lastIndexFune];
    const modal = await this.modalController.create({
      component: AddDiscountComponent,
      mode: 'ios',
      cssClass: 'modal-form',
      componentProps: {
        lastDiscountData: lastDiscount,
        initial: this.initialData
      }
    });
    const modalFune = await this.modalController.create({
      component: AddDiscountFuneComponent,
      mode: 'ios',
      cssClass: 'modal-form',
      componentProps: {
        lastDiscountData: lastDiscountFune,
        initial: this.initialData
      }
    });
    if (this.isFune) {
      await modalFune.present();
    } else {
      await modal.present();
    }

    await modal.onWillDismiss()
    .then(result => {
      if (result) {
        this.update('lot');
      }
    });
    await modalFune.onWillDismiss()
    .then(result => {
      if (result) {
        this.update('fune');
      }
    });
  }
  update(option: string) {

    if (option === 'fune') {
      this.discountService.getAllDiscount(false)
      .subscribe(
        async (res: IPaginator<IDiscount[]>) => {
          await this.dataBaseService.update(AuthConstants.DISCOUNTS_FUNE, res.data);
          await this.getAllDiscounts();
        }
      );
    } else {
      this.discountService.getAllDiscount()
      .subscribe(
        async (res: IPaginator<IDiscount[]>) => {
          await this.dataBaseService.update(AuthConstants.DISCOUNTS, res.data);
          await this.getAllDiscounts();
        }
      );
    }
  }
  async doRefresh(event) {
    if (this.isFune) {
      this.update('fune');
    } else {
      this.update('lot');
    }

    await setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

  async saveChanges(): Promise<void> {
    const load = await this.loadingController.create({
      message: 'Guardando datos'
    });
    load.present();
    const dataFromForm = this.dataForm.value;
    const dataToSave: IInitial = {
      id: 1,
      first: dataFromForm.first,
      second: dataFromForm.second,
      third: dataFromForm.third
    };
    this.initialService.updateInitial(dataToSave)
    .subscribe(
      (res: IInitial) => {
        this.initialData = res;
        this.dataForm.patchValue(res);
        this.dataBaseService.storeOneData(AuthConstants.INITIALS, res);
        load.dismiss();
      },
      (error: any) => {
        load.dismiss();
        this.alertService.presentAlert(JSON.stringify(error));
      }
    );
  }

  changeTabDiscount(index: number): void {
    this.isFune = index === 0? false:true;
  }
}
