import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingController, ModalController } from '@ionic/angular';
import { PatternEnum } from 'src/app/core/enum/pattern-enum';
import { IMemorialLot } from 'src/app/core/models/memorial-lot/memorial-lot.model';
import { AlertService } from 'src/app/core/service/alert/alert.service';
import { MemorialLotService } from 'src/app/core/service/memorial-lot/memorial-lot.service';
import { ToastService } from 'src/app/core/service/toast/toast.service';
import { FileUploadComponent } from 'src/app/shared/components/file-upload/file-upload.component';

@Component({
  selector: 'app-edit-memorial-lot',
  templateUrl: './edit-memorial-lot.component.html',
  styleUrls: ['./edit-memorial-lot.component.scss'],
})
export class EditMemorialLotComponent implements OnInit {
  @Input() dataPlace: IMemorialLot;
  placeForm: FormGroup;
  placeName: string;

  constructor(
    private modalController: ModalController,
    private formBuilder: FormBuilder,
    private memorialLotService: MemorialLotService,
    private loadingController: LoadingController,
    private toastService: ToastService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.builForm();
    this.placeForm.patchValue(this.dataPlace);
  }

  builForm(){
    this.placeForm = this.formBuilder.group({
      id: ['',[]],
      name: ['',[
        Validators.required,
        Validators.pattern(PatternEnum.PATTERN_ONLY_TEXT)
      ]],
      image: ['',[
        // Validators.required,
      ]]
    });
  }

  closeModal(){
    this.modalController.dismiss({
      save: false
    });
  }

  async updatePlace(){
    const data = this.placeForm.value;
    const messageData = 'Guardando datos, porfavor espera';
    const load = await this.loadingController.create({
      message: messageData
    });
    await load.present();
    this.memorialLotService.updatePlace(data)
    .subscribe(
      (res: IMemorialLot) => {
        const messageConfirm = `Los datos del Lugar se actualizaron correctamente`;
        load.dismiss();
        this.toastService.presentToast(messageConfirm);
        // this.modalController.dismiss();
        this.modalController.dismiss({
          save: true
        });
        console.log('UpPLace', res);
      },
      (error: any) => {
        load.dismiss();
        const headerData = 'Ooops... ocurrió un problema';
        const subHeaderData = 'Contactese con administración';
        const messageDataToAlert = JSON.stringify(error);
        this.alertService.presentAlert(headerData, subHeaderData, messageDataToAlert);
    });
  }

  async fileUpload() {
    const modal = await this.modalController.create({
      component: FileUploadComponent,
      mode: 'ios',
      componentProps: {
        category: 'places',
        userName: this.placeForm.controls.name.value
      }

    });
    await modal.present();

    await modal.onDidDismiss()
    .then(
      (res: any) => {
        const voucher = res.data.voucher;
        if (voucher) {
          this.placeForm.controls.image.setValue(voucher);
        }
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
      }
    );
  }
}
