import { Component, DoCheck, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, LoadingController, NavController } from '@ionic/angular';
import { CookieService } from 'ngx-cookie-service';
import { AuthConstants } from 'src/app/core/config/auth-constants.config';
import { IAuthResponse } from 'src/app/core/models/login/auth-response.model';
import { IMemorialLot } from 'src/app/core/models/memorial-lot/memorial-lot.model';
import { IServerResponseOnlyMessage } from 'src/app/core/models/server-response.model';
import { AlertService } from 'src/app/core/service/alert/alert.service';
import { DataPlacesService } from 'src/app/core/service/data-places/data-places.service';
import { MemorialLotService } from 'src/app/core/service/memorial-lot/memorial-lot.service';
import { ToastService } from 'src/app/core/service/toast/toast.service';

@Component({
  selector: 'app-data-memorial-lot',
  templateUrl: './data-memorial-lot.component.html',
  styleUrls: ['./data-memorial-lot.component.scss'],
})
export class DataMemorialLotComponent implements OnInit, DoCheck {
  @Input() allPlaces;
  @Output() reloadEventEmitter: EventEmitter<boolean> = new EventEmitter();
  data: IAuthResponse;

  constructor(
    private router: Router,
    private navCtrl: NavController,
    private datPlaceService: DataPlacesService,
    private memorialLotService: MemorialLotService,
    private loadingController: LoadingController,
    private toastService: ToastService,
    private alertService: AlertService,
    private alertController: AlertController,
    private cookieService: CookieService
  ) { }

  ngDoCheck(): void {
    this.data = JSON.parse(this.cookieService.get(AuthConstants.AUTH));
  }

  ngOnInit() {
    this.data = JSON.parse(this.cookieService.get(AuthConstants.AUTH));
  }

  viewDetailMemorialLot(place){
    // this.router.navigate(['detail-memorial-lot']);
    this.navCtrl.navigateForward(`home/memorial-lot/sector/${place.id}`);
    this.datPlaceService.saveData(place);
  }

  async deleteMemorialLot(data: IMemorialLot){
    const alert = await this.alertController.create({
      header: `¿Eliminar el Lote Memorial ${data.name} ?`,
      mode: 'ios',
      message: '<strong>Una vez eliminado no podrá recuperar los datos del Lote Memorial</strong>',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'btn-danger',
          id: 'cancel-button',
          handler: (blah) => {
            alert.dismiss();
          }
        }, {
          text: 'Eliminar',
          id: 'confirm-button',
          handler: () => {
            this.delete(data);
          }
        }
      ]
    });
    await alert.present();
  }

  async delete(place: IMemorialLot){
    const messageData = 'Guardando datos, porfavor espera';
    const load = await this.loadingController.create({
      message: messageData
    });
    await load.present();
    this.memorialLotService.deleteMemorialLot(place.id)
    .subscribe(
      (res: IServerResponseOnlyMessage) => {
        this.reloadEventEmitter.emit(true);
        const messageConfirm = `Se elimino correctamente el Lote Memorial`;
        load.dismiss();
        this.toastService.presentToast(messageConfirm);
        console.log('Eliminado', res);
      },
      (error: any) => {
        load.dismiss();
        const headerData = 'Ooops... ocurrió un problema';
        const subHeaderData = 'Contactese con administración';
        const messageDataToAlert = JSON.stringify(error);
        this.alertService.presentAlert(headerData, subHeaderData, messageDataToAlert);
      });

  }

}
