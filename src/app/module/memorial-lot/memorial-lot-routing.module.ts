import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MemorialLotPage } from './memorial-lot.page';

const routes: Routes = [
  {
    path: '',
    component: MemorialLotPage
  },
  {
    path: 'sector/:id',
    loadChildren: () => import('./detail-memorial-lot/detail-memorial-lot.module').then( m => m.DetailMemorialLotPageModule)
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MemorialLotPageRoutingModule {}
