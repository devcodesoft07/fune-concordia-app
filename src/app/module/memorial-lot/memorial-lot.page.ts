import { Component, DoCheck, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { CookieService } from 'ngx-cookie-service';
import { AuthConstants } from 'src/app/core/config/auth-constants.config';
import { IAuthResponse } from 'src/app/core/models/login/auth-response.model';
import { IMemorialLot } from 'src/app/core/models/memorial-lot/memorial-lot.model';
import { IPaginator } from 'src/app/core/models/paginator.model';
import { DataBaseService } from 'src/app/core/service/data-base/data-base.service';
import { DataPlacesService } from 'src/app/core/service/data-places/data-places.service';
import { MemorialLotService } from 'src/app/core/service/memorial-lot/memorial-lot.service';
import { AddMemorialLotComponent } from './components/add-memorial-lot/add-memorial-lot.component';

@Component({
  selector: 'app-memorial-lot',
  templateUrl: './memorial-lot.page.html',
  styleUrls: ['./memorial-lot.page.scss'],
})
export class MemorialLotPage implements OnInit, DoCheck {
  allPlaces: IMemorialLot[];
  data: IAuthResponse;

  constructor(
    private modalController: ModalController,
    private memorialLotService: MemorialLotService,
    private cookieService: CookieService,
    private dataBaseService: DataBaseService,
  ) { }

  ngDoCheck(): void {
    this.data = JSON.parse(this.cookieService.get(AuthConstants.AUTH));
  }

  ngOnInit() {
    this.getAllPlacesOfMemorialLot();
  }

  async addMemorialLot(){
      const modal = await this.modalController.create({
        component: AddMemorialLotComponent,
        cssClass: 'modal-form',
        componentProps: {
          memorialLot: 'Tiquipaya'
        }
      });
      await modal.present();

      await modal.onWillDismiss()
      .then(result => {
      this.update();
    });
  }

  async getAllPlacesOfMemorialLot(){
    this.dataBaseService.existKey(AuthConstants.PLACES).subscribe(
      (isExist: boolean) => {
        if (!(isExist)) {
          this.memorialLotService.getAllPlaces()
          .subscribe(
            (res: IPaginator<IMemorialLot[]>) => {
              this.allPlaces = res.data;
              this.dataBaseService.storeAllData(AuthConstants.PLACES, res.data);
            },
            (error: any) => {
              console.log(error);
            }
          );
        } else {
          this.dataBaseService.getData(AuthConstants.PLACES).subscribe(
            (res: any) => {
              this.allPlaces = res;
            }
          );
        }
      }
    );
  }

  update() {
    this.memorialLotService.getAllPlaces()
    .subscribe(
      async (res: IPaginator<IMemorialLot[]>) => {
        await this.dataBaseService.update(AuthConstants.PLACES, res.data);
        await this.getAllPlacesOfMemorialLot();
      }
    );
  }

  async doRefresh(event) {
    await this.update();
    await setTimeout(() => {
      event.target.complete();
    }, 2000);
  }
}
