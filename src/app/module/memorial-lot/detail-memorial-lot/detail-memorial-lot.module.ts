import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
//Modules
import { DetailMemorialLotPageRoutingModule } from './detail-memorial-lot-routing.module';
import { SwiperModule } from 'swiper/angular';
import { AvatarModule } from 'ngx-avatar';
import { AngularMaterialModule } from 'src/app/shared/angular-material/angular-material.module';
import { SharedModule } from 'src/app/shared/shared.module';
//Components, container, pages
import { DetailMemorialLotPage } from './detail-memorial-lot.page';
import { DataSectorListComponent } from './container/data-sector-list/data-sector-list.component';
import { AddSectorComponent } from './components/add-sector/add-sector.component';
import { EditSectorComponent } from './components/edit-sector/edit-sector.component';
import { QuoterComponent } from './components/quoter/quoter.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    DetailMemorialLotPageRoutingModule,
    AvatarModule,
    AngularMaterialModule,
    SharedModule,
    SwiperModule
  ],
  declarations: [
    DetailMemorialLotPage,
    DataSectorListComponent,
    AddSectorComponent,
    EditSectorComponent,
    QuoterComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DetailMemorialLotPageModule {}
