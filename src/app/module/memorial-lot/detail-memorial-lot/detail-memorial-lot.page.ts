import { SectorService } from './../../../core/service/sector/sector.service';
import { ModalController } from '@ionic/angular';
import { Component, DoCheck, OnInit } from '@angular/core';
import { AddSectorComponent } from './components/add-sector/add-sector.component';
import { DataPlacesService } from 'src/app/core/service/data-places/data-places.service';
import { IMemorialLot } from 'src/app/core/models/memorial-lot/memorial-lot.model';
import { IPaginator } from 'src/app/core/models/paginator.model';
import { ISector } from 'src/app/core/models/memorial-lot/sector.model';
import { EditMemorialLotComponent } from '../components/edit-memorial-lot/edit-memorial-lot.component';
import { MemorialLotService } from 'src/app/core/service/memorial-lot/memorial-lot.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
import { AuthConstants } from 'src/app/core/config/auth-constants.config';
import { IAuthResponse } from 'src/app/core/models/login/auth-response.model';
import { DataBaseService } from 'src/app/core/service/data-base/data-base.service';

@Component({
  selector: 'app-detail-memorial-lot',
  templateUrl: './detail-memorial-lot.page.html',
  styleUrls: ['./detail-memorial-lot.page.scss'],
})
export class DetailMemorialLotPage implements OnInit, DoCheck {
  dataPlace: IMemorialLot;
  listSectorsOfPlace: ISector[];
  coinStatus = true;
  placeId: number;
  data: IAuthResponse;
  state: boolean;

  constructor(
    private modalController: ModalController,
    private sectorService: SectorService,
    private memorialLotService: MemorialLotService,
    private activateRoute: ActivatedRoute,
    private cookieService: CookieService,
    private dataBaseService: DataBaseService,
  ) {
    this.activateRoute.params.subscribe(
      (res: Params) => {
        this.placeId = res.id;
      }
    );
  }
  ngDoCheck(): void {
    this.data = JSON.parse(this.cookieService.get(AuthConstants.AUTH));
  }

  ngOnInit() {
    this.getPlace();
    this.getAllSectorsOfPlace();
  }

  async getAllSectorsOfPlace(){
    this.state = true;
    this.dataBaseService.existKey(AuthConstants.PLACES + '/' + this.placeId).subscribe(
      (isExist: boolean ) => {
        if (!(isExist)) {
          this.sectorService.getAllSectorsOfPlace(this.placeId)
          .subscribe(
            (res: IPaginator<ISector[]>) => {
              this.state = false;
              this.listSectorsOfPlace = res.data;
              this.dataBaseService.storeAllData(AuthConstants.PLACES + '/' + this.placeId, res.data);
            }
          );
        } else {
          this.dataBaseService.getData(AuthConstants.PLACES + '/' + this.placeId).subscribe(
            (res: any) => {
              this.listSectorsOfPlace = res;
              this.state = false;
            }
          );
        }
      }
    );
  }

  getPlace() {
    this.state = true;
    this.memorialLotService.getPlace(this.placeId)
    .subscribe(
      (res: IMemorialLot) => {
        this.state = false;
        this.dataPlace = res;
      }
    );
  }

  async addSector(){
    const modal = await this.modalController.create({
      component: AddSectorComponent,
      cssClass: 'modal-form',
      componentProps: {
        dataPlace: this.dataPlace
      }
    });
    await modal.present();

    await modal.onWillDismiss()
    .then(result => {
      this.reloadSectors();
    });
  }
  async editPlace(){
    const modal = await this.modalController.create({
      component: EditMemorialLotComponent,
      cssClass: 'modal-form',
      componentProps: {
        dataPlace: this.dataPlace
      }
    });
    await modal.present();

    await modal.onWillDismiss()
    .then(result => {
      this.getPlace();
    });
  }

  coinChange(event){
    this.coinStatus = event.detail.checked;
  }

  reloadSectors(): void {
    this.sectorService.getAllSectorsOfPlace(this.placeId)
    .subscribe(
      async (res: IPaginator<ISector[]>) => {
        await this.dataBaseService.update(AuthConstants.PLACES + '/' + this.placeId, res.data);
        await this.getAllSectorsOfPlace();
      }
    );
  }

  async doRefresh(event) {
    this.reloadSectors();
    this.getPlace();
    await setTimeout(() => {
      event.target.complete();
    }, 2000);
  }

}
