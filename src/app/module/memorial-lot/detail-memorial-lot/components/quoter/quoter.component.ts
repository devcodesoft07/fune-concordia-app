/* eslint-disable @typescript-eslint/naming-convention */
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NavController, SelectCustomEvent } from '@ionic/angular';
import { AuthConstants } from 'src/app/core/config/auth-constants.config';
import { IDiscount } from 'src/app/core/models/discount/discount-model';
import { ISector } from 'src/app/core/models/memorial-lot/sector.model';
import { IDataToQuote } from 'src/app/core/models/quotes/quote.model';
import { DataBaseService } from 'src/app/core/service/data-base/data-base.service';
import { DataServiceQuoteService } from 'src/app/core/service/data-service-cuote/data-service-quote.service';
import SwiperCore, { Autoplay, Navigation, Pagination, SwiperOptions } from 'swiper';
SwiperCore.use([Navigation, Pagination, Autoplay]);

@Component({
  selector: 'app-quoter',
  templateUrl: './quoter.component.html',
  styleUrls: ['./quoter.component.scss']
})
export class QuoterComponent implements OnInit {
  @Input() sectorsList: ISector[];
  discountsList: IDiscount[];
  discount: IDiscount;
  sector: ISector;
  sectorQuoterForm: FormGroup;
  config: SwiperOptions = {
    slidesPerView: 1,
    spaceBetween: 50,
    navigation: true,
    pagination: { clickable: true },
    scrollbar: { draggable: true },
    autoplay: true
  };
  constructor(
    private formBuilder: FormBuilder,
    private dataBaseService: DataBaseService,
    private dataCuoteService: DataServiceQuoteService,
    private navController: NavController,
  ) {
    this.buildForm();
  }

  buildForm(): void {
    this.sectorQuoterForm = this.formBuilder.group({
      data: [''],
      discount: [''],
      old_customer: [false],
      crematorium_funeral: [false],
      closed_funeral: [false],
      title: ['Sectores']
    });
  }

  async ngOnInit() {
    this.sector = this.sectorsList[0];
    this.dataBaseService.getData(AuthConstants.DISCOUNTS).subscribe(
      async (res: any) => {
        this.discountsList = this.generateNewDiscountList(res);
        this.discount = await this.discountsList[0];
        await this.sectorQuoterForm.patchValue({
          data: this.sector,
          discount: this.discount
        });
      }
    );
  }

  cotizationOfSector(): void {
    const dataToCuote: IDataToQuote<ISector> = this.sectorQuoterForm.value;
    const dataItem: ISector = this.sectorQuoterForm.controls.data.value;
    this.dataBaseService.storeOneData(AuthConstants.DATATOCUOTE, dataToCuote);
    this.dataCuoteService.sendObjectSource(dataToCuote);
    this.navController.navigateForward(`home/funeral-service-quote/${dataItem.id}`);
  }

  changeDataSelected(e: any, option: string): void {
    switch (option) {
      case 's':
        this.sector = e.detail.value;
        break;
      case 'd':
        this.discount = e.detail.value;
        break;
      default:
        this.sector = this.sectorsList[0];
        this.discount = this.discountsList[0];
        break;
    }
  }

  generateNewDiscountList(res: any[]): IDiscount[] {
    return res.filter(
      (discount: IDiscount) => discount.lot_value !== 1
    );
  }

}
