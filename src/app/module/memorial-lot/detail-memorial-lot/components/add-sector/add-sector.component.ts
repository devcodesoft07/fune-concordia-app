/* eslint-disable @typescript-eslint/naming-convention */
import { Component, Input, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingController, ModalController } from '@ionic/angular';
import { IMemorialLot } from 'src/app/core/models/memorial-lot/memorial-lot.model';
import { ISector, ISectorSave } from 'src/app/core/models/memorial-lot/sector.model';
import { AlertService } from 'src/app/core/service/alert/alert.service';
import { SectorService } from 'src/app/core/service/sector/sector.service';
import { ToastService } from 'src/app/core/service/toast/toast.service';
import { FileUploadComponent } from 'src/app/shared/components/file-upload/file-upload.component';

@Component({
  selector: 'app-add-sector',
  templateUrl: './add-sector.component.html',
  styleUrls: ['./add-sector.component.scss'],
})
export class AddSectorComponent implements OnInit {
  @Input() dataPlace: IMemorialLot;
  sectorForm: FormGroup;
  images: string[] = [];
  state: boolean;

  constructor(
    private modalController: ModalController,
    private formuBuilder: FormBuilder,
    private sectorService: SectorService,
    private loadingController: LoadingController,
    private toastService: ToastService,
    private alertService: AlertService,
    private storage: AngularFireStorage
    ) { }

  ngOnInit() {
    this.buildForm();
    this.sectorForm.controls.place_id.setValue(this.dataPlace.id);
  }

  buildForm(){
    this.sectorForm = this.formuBuilder.group({
      name: ['',[
        Validators.required
      ]],
      real_price: ['',[
        // Validators.required
      ]],
      discount_price: ['',[
        // Validators.required
      ]],
      predicted_price: ['',[
        Validators.required
      ]],
      image: [['empty', 'empty1'],[]],
      place_id: ['',[
        Validators.required
      ]]
    });
  }

  closeModal(){
    this.modalController.dismiss();
  }

  async addSector(){
    const realPrice = Math.round(this.sectorForm.controls.predicted_price.value * 1.24);
    const priceWithDiscount = (realPrice-this.sectorForm.controls.predicted_price.value);
    this.sectorForm.controls.real_price.setValue(realPrice);
    this.sectorForm.controls.discount_price.setValue(priceWithDiscount);
    const dataFromForm = this.sectorForm.value;
    const data: ISectorSave =  {
      name: dataFromForm.name,
      real_price: dataFromForm.real_price,
      discount_price: dataFromForm.discount_price,
      predicted_price: dataFromForm.predicted_price,
      image: this.generateImages(dataFromForm.image),
      place_id: this.dataPlace.id
    };
    const messageData = 'Guardando datos, porfavor espera';
    const load = await this.loadingController.create({
      message: messageData
    });
    await load.present();
    this.sectorService.addSectorForPlace(data)
    .subscribe(
      (res: ISector) => {
        const messageConfirm = `Se agrego el nuevo sector correctamente`;
        load.dismiss();
        this.toastService.presentToast(messageConfirm);
        this.closeModal();
      },
      (error: any) => {
        load.dismiss();
        const headerData = 'Ooops... ocurrió un problema';
        const subHeaderData = 'Contactese con administración';
        const messageDataToAlert = JSON.stringify(error);
        this.alertService.presentAlert(headerData, subHeaderData, messageDataToAlert);
      });
  }

  async fileUpload() {
    const modal = await this.modalController.create({
      component: FileUploadComponent,
      mode: 'ios',
      componentProps: {
        category: 'Sectors',
        userName: this.sectorForm.controls.name.value
      }

    });
    await modal.present();

    await modal.onDidDismiss()
    .then(
      (res: any) => {
        const voucher = res.data.voucher;
        if (voucher) {
          this.images.push(voucher);
          this.sectorForm.controls.image.setValue(this.images);
        }
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
      }
    );
  }

  deleteImage(image: string, index: number): void {
    this.state = true;
    this.storage.refFromURL(image).delete()
    .subscribe(
      (res: any) => {
        this.state = true;
        this.images.splice(index, 1);
        this.sectorForm.controls.image.setValue(this.images);
      }
    );
  }

  generateImages(images: string[]): string[] {
    const firstImage: string = images[0];
    if (firstImage === 'empty') {
      return images;
    } else {
      const size: number = images.length;
      const count: number = 2 - size;
      if (size < 2) {
        for (let index = 0; index < count; index++) {
          images.push('empty' + index);
        }
      }
      return images;
    }
  }
}
