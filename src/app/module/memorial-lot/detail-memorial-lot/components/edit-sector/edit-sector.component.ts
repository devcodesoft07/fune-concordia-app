/* eslint-disable @typescript-eslint/naming-convention */
import { Component, Input, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingController, ModalController } from '@ionic/angular';
import { IImage } from 'src/app/core/models/memorial-lot/image.model';
import { IMemorialLot } from 'src/app/core/models/memorial-lot/memorial-lot.model';
import { ISector } from 'src/app/core/models/memorial-lot/sector.model';
import { IServerResponse } from 'src/app/core/models/server-response.model';
import { AlertService } from 'src/app/core/service/alert/alert.service';
import { SectorService } from 'src/app/core/service/sector/sector.service';
import { ToastService } from 'src/app/core/service/toast/toast.service';
import { FileUploadComponent } from 'src/app/shared/components/file-upload/file-upload.component';

@Component({
  selector: 'app-edit-sector',
  templateUrl: './edit-sector.component.html',
  styleUrls: ['./edit-sector.component.scss'],
})
export class EditSectorComponent implements OnInit {
  @Input() sector: ISector;
  @Input() dataPlace: IMemorialLot;
  sectorForm: FormGroup;
  imageForm: FormGroup;
  allImages: IImage[];
  state: boolean;

  constructor(
    private modalController: ModalController,
    private formBuilder: FormBuilder,
    private sectorService: SectorService,
    private loadingController: LoadingController,
    private toastService: ToastService,
    private alertService: AlertService,
    private storage: AngularFireStorage
  ) {
    this.buildForm();
   }

   get allImagesSector() { return (this.allImages && this.allImages[0]) ? this.allImages : null; }
  ngOnInit() {
    this.getAllImages();
    this.sectorForm.patchValue(this.sector);
    this.sectorForm.controls.place_id.setValue(this.dataPlace.id);
  }

  buildForm(){
    this.sectorForm = this.formBuilder.group({
      id: ['',[
        Validators.required
      ]],
      name: ['',[
        Validators.required
      ]],
      real_price: ['',[
        Validators.required
      ]],
      discount_price: ['',[
        Validators.required
      ]],
      image: ['',[]],
      predicted_price: ['',[
        Validators.required
      ]],
      place_id: ['',[
        Validators.required
      ]]
    });

    this.imageForm = this.formBuilder.group({
      id: ['',[]],
      image: ['',[]]
    });
  }

  closeModal(){
    this.modalController.dismiss();
  }

  async getAllImages(){
    this.state = true;
    this.sectorService.getAllImagesOfSector(this.sector.id)
    .subscribe(
      (res: IServerResponse<IImage[]>)=>{
        this.state = false;
        this.allImages = res.data;
      }
    );
  }

  async updateSector(){
    const realPrice = Math.round(this.sectorForm.controls.predicted_price.value * 1.24);
    const priceWithDiscount = (realPrice-this.sectorForm.controls.predicted_price.value);
    this.sectorForm.controls.real_price.setValue(realPrice);
    this.sectorForm.controls.discount_price.setValue(priceWithDiscount);
    const data: ISector = this.sectorForm.value;
    const messageData = 'Guardando datos, porfavor espera';
    const load = await this.loadingController.create({
      message: messageData
    });
    await load.present();
    this.sectorService.updateSectorOfPlace(data)
    .subscribe(
      (res: ISector) => {
        const messageConfirm = `Se actulizaron los datos del sector correctamente`;
        load.dismiss();
        this.toastService.presentToast(messageConfirm);
        this.closeModal();
      },
      (error: any) => {
        load.dismiss();
        const headerData = 'Ooops... ocurrió un problema';
        const subHeaderData = 'Contactese con administración';
        const messageDataToAlert = JSON.stringify(error);
        this.alertService.presentAlert(headerData, subHeaderData, messageDataToAlert);
      });
  }

  async fileUpload(){
    console.log(this.getImageEmpty());
    const image: IImage = this.getImageEmpty();
    const modal = await this.modalController.create({
      component: FileUploadComponent,
      mode: 'ios',
      componentProps: {
        category: 'Sectors',
        userName: this.sectorForm.controls.name.value
      }

    });
    await modal.present();

    await modal.onDidDismiss()
    .then(
      (res: any) => {
        const voucher = res.data.voucher;
        if (voucher) {
          this.imageForm.controls.id.setValue(image.id);
          this.imageForm.controls.image.setValue(voucher);
          const data = this.imageForm.value;
          this.sectorService.updateImageOfSector(this.sector.id, data)
          .subscribe(
            (response: IImage) =>{
              this.getAllImages();
            }
          );
        }
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
      }
    );
  }


  deleteImage(image: IImage, index: number): void {
    this.state = true;
    const data: IImage = {
      id: image.id,
      image: 'empty' + index
    };
    this.sectorService.updateImageOfSector(this.dataPlace.id, data)
    .subscribe(
      (response: IImage) =>{
        this.storage.refFromURL(image.image).delete()
        .subscribe(
          (res: any) => {
            this.state = true;
            this.getAllImages();
          }
        );
      }
    );
  }

  getImageEmpty(): IImage {
    let image: IImage = {
      image: 'empty',
      id: 0
    };
    let isFoundImageEmpty = false;
    this.allImages.forEach(element => {
      if (element.image.includes('empty') || element.image === '') {
        if (!isFoundImageEmpty) {
          image = element;
          isFoundImageEmpty = true;
        }
      }
    });
    return image;
  }
}
