import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AlertController, LoadingController, ModalController } from '@ionic/angular';
import { IMemorialLot } from 'src/app/core/models/memorial-lot/memorial-lot.model';
import { ISector } from 'src/app/core/models/memorial-lot/sector.model';
import { AlertService } from 'src/app/core/service/alert/alert.service';
import { ColorService } from 'src/app/core/service/color/color.service';
import { SectorService } from 'src/app/core/service/sector/sector.service';
import { ToastService } from 'src/app/core/service/toast/toast.service';
import { EditSectorComponent } from '../../components/edit-sector/edit-sector.component';

@Component({
  selector: 'app-data-sector-list',
  templateUrl: './data-sector-list.component.html',
  styleUrls: ['./data-sector-list.component.scss'],
})
export class DataSectorListComponent implements OnInit {
  @Input() dataPlace: IMemorialLot;
  @Input() listSectorsOfPlace: any;
  @Input() coinStatus;
  @Output() reloadEventEmitter: EventEmitter<boolean> = new EventEmitter();
  // sectors: ISector[];
  displayedColumns: string[] = ['name', 'real_price', 'discount_price', 'predicted_price','action'];
  // displayedColumns: string[] = ['deadline', 'lot', 'funeral', 'quota-6', 'quota-10', 'quota-20', 'cash', 'action'];

  constructor(
    public colorService: ColorService,
    private modalController: ModalController,
    private sectorService: SectorService,
    private loadingController: LoadingController,
    private toastService: ToastService,
    private alertService: AlertService,
    private alertController: AlertController
  ) { }

  ngOnInit() {
    console.log('moneda', this.coinStatus);

  }

  async editSector(element){
    const modal = await this.modalController.create({
      component: EditSectorComponent,
      cssClass: 'modal-form',
      componentProps: {
        sector: element,
        dataPlace: this.dataPlace
      }
    });
    await modal.present();

    await modal.onWillDismiss()
      .then(result => {
      this.reloadEventEmitter.emit(true);
    });
  }

  async deleteSector(data: ISector){
    const alert = await this.alertController.create({
      header: `¿Eliminar el sector ${data.name} ?`,
      mode: 'ios',
      message: '<strong>Una vez eliminado no podrá recuperar los datos del Sector</strong>',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'btn-danger',
          id: 'cancel-button',
          handler: (blah) => {
            alert.dismiss();
          }
        }, {
          text: 'Eliminar',
          id: 'confirm-button',
          handler: () => {
            this.delete(data);
          }
        }
      ]
    });
    await alert.present();
  }

  async delete(element){
    console.log('idElm', element.id);
    const messageData = 'Guardando datos, porfavor espera';
    const load = await this.loadingController.create({
      message: messageData
    });
    await load.present();
    this.sectorService.deleteSector(element.id)
    .subscribe(
      (res: boolean) => {
        this.reloadEventEmitter.emit(true);
        const messageConfirm = `Se elimino el sector correctamente`;
        load.dismiss();
        this.toastService.presentToast(messageConfirm);
        console.log('Eliminado', res);
      },
      (error: any) => {
        load.dismiss();
        const headerData = 'Ooops... ocurrió un problema';
        const subHeaderData = 'Contactese con administración';
        const messageDataToAlert = JSON.stringify(error);
        this.alertService.presentAlert(headerData, subHeaderData, messageDataToAlert);
      });
  }
}
