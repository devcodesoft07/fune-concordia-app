import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailMemorialLotPage } from './detail-memorial-lot.page';

const routes: Routes = [
  {
    path: '',
    component: DetailMemorialLotPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailMemorialLotPageRoutingModule {}
