import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MemorialLotPageRoutingModule } from './memorial-lot-routing.module';

import { MemorialLotPage } from './memorial-lot.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { DataMemorialLotComponent } from './container/data-memorial-lot/data-memorial-lot.component';
import { AddMemorialLotComponent } from './components/add-memorial-lot/add-memorial-lot.component';
import { EditMemorialLotComponent } from './components/edit-memorial-lot/edit-memorial-lot.component';
import { AvatarModule } from 'ngx-avatar';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AngularMaterialModule } from 'src/app/shared/angular-material/angular-material.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MemorialLotPageRoutingModule,
    SharedModule,
    AvatarModule,
    NgxDatatableModule,
    ReactiveFormsModule,
    AngularMaterialModule
  ],
  declarations: [
    MemorialLotPage,
    DataMemorialLotComponent,
    AddMemorialLotComponent,
    EditMemorialLotComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class MemorialLotPageModule {}
