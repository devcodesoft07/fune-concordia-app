/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @angular-eslint/component-class-suffix */
import { Component, DoCheck, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IFuneralService } from 'src/app/core/models/funeral-service/funeralService.model';
import { ISector } from 'src/app/core/models/memorial-lot/sector.model';
import { IOption } from 'src/app/core/models/option.model';
import { IDataToQuote, IFinalPrice, IRegularPrice } from 'src/app/core/models/quotes/quote.model';
import { DataServiceQuoteService } from 'src/app/core/service/data-service-cuote/data-service-quote.service';

@Component({
  selector: 'app-card-final-quote',
  templateUrl: './card-final-quote.container.html',
  styleUrls: ['./card-final-quote.container.scss'],
})
export class CardFinalQuoteContainer implements OnInit, DoCheck {
  @Input() dataQuote: IDataToQuote<ISector | IFuneralService>;
  @Input() initialPay: number;
  @Input() coinStatus: boolean;
  @Input() option: IOption;
  @Output() totalDiscountEvenEmitter: EventEmitter<number> = new EventEmitter();
  @Output() totalFinalQuote: EventEmitter<number> = new EventEmitter();
  finalPrice: IFinalPrice;
  factorValue: number;
  totalValueDiscount: number;
  totalPrice: number;
  totalValueCash: number;
  agreedPrice: number;
  constructor(
    private dataServiceQuoteService: DataServiceQuoteService
  ) {

  }

  ngOnInit() {
    this.calculateFinalPrice();
  }

  ngDoCheck(): void {
    this.dataServiceQuoteService.totalDiscount$.subscribe(
      (res: number) => {
        this.totalValueDiscount = res;
        this.totalDiscountEvenEmitter.emit(res);
        if(this.dataQuote.title === 'Sectores'){
          this.factorValue = this.dataQuote.discount.lot_value;
        }else{
          this.factorValue = this.dataQuote.discount.fune_value;
        }
        this.calculateFinalPrice();
      }
    ).unsubscribe();
  }


  calculateFinalPrice(){
    let totalPrice = Math.ceil((this.dataQuote.data.predicted_price * 1));

    if (this.dataQuote.crematorium_funeral) {
      totalPrice += (this.option.fune_cremated * 1);
    }
    if (this.dataQuote.closed_funeral) {
      totalPrice -= (this.option.fune_closed * 1);
    }
    this.totalPrice = totalPrice;
    const finalBalance = Math.ceil(totalPrice - this.totalValueDiscount - this.initialPay);
    let monthlyFee =  Math.ceil(finalBalance / (12 * this.dataQuote.discount.term ));
    if (this.factorValue !== 0  && this.factorValue !== 1) {
      monthlyFee = Math.ceil((finalBalance * this.factorValue));
    }
    this.totalValueCash = totalPrice * (1 - this.dataQuote.discount.cash / 100);
    this.agreedPrice = totalPrice - this.totalValueDiscount;
    this.totalFinalQuote.emit(totalPrice);
    this.finalPrice = {
       final_balanced: finalBalance,
       initial_quote: this.initialPay,
       total_discount: this.totalValueDiscount,
       monthly_fee: monthlyFee,
    };
  }


}
