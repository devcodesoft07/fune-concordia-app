/* eslint-disable @typescript-eslint/naming-convention */
import { Component, DoCheck, Input, OnInit } from '@angular/core';
import { IFuneralService } from 'src/app/core/models/funeral-service/funeralService.model';
import { IInitial } from 'src/app/core/models/initial.model';
import { ISector } from 'src/app/core/models/memorial-lot/sector.model';
import { IOption } from 'src/app/core/models/option.model';
import { IDataToQuote, IRegularPrice } from 'src/app/core/models/quotes/quote.model';

@Component({
  selector: 'app-card-regular-price',
  templateUrl: './card-regular-price.component.html',
  styleUrls: ['./card-regular-price.component.scss'],
})
export class CardRegularPriceComponent implements DoCheck {
  @Input() dataQuote: IDataToQuote<ISector | IFuneralService>;
  @Input() initial: IInitial;
  @Input() coinStatus: boolean;
  @Input() option: IOption;
  regularPrice: IRegularPrice;
  totalValueCash: number;
  constructor() {
  }

  ngDoCheck(): void {
    this.generateRegularPriceData();
  }

  generateRegularPriceData() {
    let totalPrice = Math.ceil((this.dataQuote.data.predicted_price * 1));
    if (this.dataQuote.crematorium_funeral) {
      totalPrice += (this.option.fune_cremated * 1);
    }
    if (this.dataQuote.closed_funeral) {
      totalPrice -= (this.option.fune_closed * 1);
    }
    this.totalValueCash = totalPrice * (1 - this.dataQuote.discount.cash / 100);
    this.regularPrice = {
      emergency_price: Math.ceil(this.dataQuote.data.real_price),
      money_save: Math.ceil(this.dataQuote.data.discount_price),
      totalPrice: Math.ceil(totalPrice),
      initial_quote: Math.ceil(totalPrice * (this.initial.first / 100)),
      second_intial_quote: Math.ceil(totalPrice * (this.initial.second / 100)),
      third_initial_quote: Math.ceil(totalPrice * (this.initial.third / 100)),
    };
  }
}
