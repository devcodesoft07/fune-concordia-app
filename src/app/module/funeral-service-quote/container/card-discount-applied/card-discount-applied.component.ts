import { Component, DoCheck, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { IFuneralService } from 'src/app/core/models/funeral-service/funeralService.model';
import { IInitial } from 'src/app/core/models/initial.model';
import { ISector } from 'src/app/core/models/memorial-lot/sector.model';
import { IOption } from 'src/app/core/models/option.model';
import { IDataToQuote } from 'src/app/core/models/quotes/quote.model';
import { DataBaseService } from 'src/app/core/service/data-base/data-base.service';
import { DataServiceQuoteService } from 'src/app/core/service/data-service-cuote/data-service-quote.service';

@Component({
  selector: 'app-card-discount-applied',
  templateUrl: './card-discount-applied.component.html',
  styleUrls: ['./card-discount-applied.component.scss'],
})
export class CardDiscountAppliedComponent implements OnInit, DoCheck {
  @Input() dataQuote: IDataToQuote<ISector | IFuneralService>;
  @Input() initialPay: number;
  @Input() initial: IInitial;
  @Input() option: IOption;
  @Input() coinStatus: boolean;
  @Output() totalDiscountEvenEmitter: EventEmitter<number> = new EventEmitter();
  firstRange: number;
  secondRange: number;
  thirdRange: number;
  factorValue: number;
  totalDiscount: number ;
  totalValueDiscount: number;
  totalPrice: number;
  totalValueCash: number;
  constructor(
    private dataServiceQuoteService: DataServiceQuoteService,
  ) { }

  ngOnInit() {
  }

  ngDoCheck(): void {
    if(this.dataQuote.title === 'Sectores'){
      this.factorValue = this.dataQuote.discount.lot_value;
    }else{
      this.factorValue = this.dataQuote.discount.fune_value;
    }
    this.calculateRange();
    this.calculateTotalDiscount();
  }

  calculateRange(){
    let totalPrice = Math.ceil((this.dataQuote.data.predicted_price * 1));
    if (this.dataQuote.crematorium_funeral) {
      totalPrice += (this.option.fune_cremated * 1);
    }
    if (this.dataQuote.closed_funeral) {
      totalPrice -= (this.option.fune_closed * 1);
    }
    this.totalPrice =  (totalPrice * 1);
    this.firstRange = Math.ceil(this.totalPrice*(this.initial.first / 100));
    this.secondRange = Math.ceil(this.totalPrice*(this.initial.second / 100));
    this.thirdRange = Math.ceil(this.totalPrice*(this.initial.third / 100));
    this.totalValueCash = totalPrice * (1 - this.dataQuote.discount.cash / 100);
  }

  calculateTotalDiscount(){
    const valueOnDiscount: number  = this.dataQuote.old_customer ? this.option.old_customer : 0;

    if(this.initialPay < this.firstRange){
      this.totalDiscount = valueOnDiscount;
    }

    if(this.firstRange <= this.initialPay && this.initialPay < this.secondRange){
      this.totalDiscount = this.dataQuote.discount.initial_6 + valueOnDiscount;
    }

    if(this.secondRange <= this.initialPay && this.initialPay < this.thirdRange){
      this.totalDiscount = this.dataQuote.discount.initial_10 + valueOnDiscount;
    }

    if(this.initialPay >= this.thirdRange){
      this.totalDiscount = this.dataQuote.discount.initial_20 + valueOnDiscount;
    }

    this.totalValueDiscount = this.totalPrice * (this.totalDiscount/100);

    this.totalDiscountEvenEmitter.emit(this.totalValueDiscount);
    this.dataServiceQuoteService.sendObjectTotalDiscount(this.totalValueDiscount);
  }

}
