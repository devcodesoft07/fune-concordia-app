import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FuneralServiceQuotePageRoutingModule } from './funeral-service-quote-routing.module';

import { FuneralServiceQuotePage } from './funeral-service-quote.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { CardFinalQuoteContainer } from './container/card-final-quote/card-final-quote.container';
import { AddFuneralServiceQuoteComponent } from './components/add-funeral-service-quote/add-funeral-service-quote.component';
import { SwiperModule } from 'swiper/angular';
import { CardRegularPriceComponent } from './container/card-regular-price/card-regular-price.component';
import { CardDiscountAppliedComponent } from './container/card-discount-applied/card-discount-applied.component';
import { SendQuoteComponent } from './components/send-quote/send-quote.component';
import { AngularMaterialModule } from 'src/app/shared/angular-material/angular-material.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    FuneralServiceQuotePageRoutingModule,
    SharedModule,
    SwiperModule,
    AngularMaterialModule
  ],
  declarations: [
    FuneralServiceQuotePage,
    CardFinalQuoteContainer,
    AddFuneralServiceQuoteComponent,
    CardRegularPriceComponent,
    CardDiscountAppliedComponent,
    SendQuoteComponent
  ]
})
export class FuneralServiceQuotePageModule {}
