import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FuneralServiceQuotePage } from './funeral-service-quote.page';

const routes: Routes = [
  {
    path: '',
    component: FuneralServiceQuotePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FuneralServiceQuotePageRoutingModule {}
