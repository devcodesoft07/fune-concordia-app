/* eslint-disable id-blacklist */
import { Component, Input, OnInit } from '@angular/core';
import { AlertController, LoadingController, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Contact, PhoneNumber, Contacts, NewContact } from '@capacitor-community/contacts';
import { ToastService } from 'src/app/core/service/toast/toast.service';
import { DataBaseService } from 'src/app/core/service/data-base/data-base.service';
import { AuthConstants } from 'src/app/core/config/auth-constants.config';
import { IDataToQuote, IQuotaServer } from 'src/app/core/models/quotes/quote.model';
import { ISector } from 'src/app/core/models/memorial-lot/sector.model';
import { IFuneralService } from 'src/app/core/models/funeral-service/funeralService.model';
import { DataServiceQuoteService } from 'src/app/core/service/data-service-cuote/data-service-quote.service';

@Component({
  selector: 'app-send-quote',
  templateUrl: './send-quote.component.html',
  styleUrls: ['./send-quote.component.scss'],
})
export class SendQuoteComponent implements OnInit {
  @Input() data: IDataToQuote<ISector | IFuneralService>;
  @Input() initial: any;
  contacts: Contact[];
  contactForm: FormGroup;
  existNumberPhone = false;
  constructor(
    private modalController: ModalController,
    private router: Router,
    private formBuilder: FormBuilder,
    private toastService: ToastService,
    private dataBaseService: DataBaseService,
    private loadController: LoadingController,
    private alertController: AlertController,
    private dataServiceQuoteService: DataServiceQuoteService,
  ) {
    this.buildForm();
  }
  buildForm() {
    this.contactForm = this.formBuilder.group({
      name: [''],
      phone: ['']
    });
  }

  ngOnInit() {
  }

  async sendInfo() {
    this.dataServiceQuoteService.addProduct(this.data);
    const numberPhone = this.contactForm.controls.phone.value;
    if (numberPhone !== '') {
      this.saveNewContact();
      this.modalController.dismiss();
    } else {
      this.toastService.presentToast('No se registró nuevo contacto');
      this.router.navigate(['/home/view-cotization']);
      this.modalController.dismiss();
    }
  }

  async saveNewContact() {
    const load = await this.loadController.create({
      message: 'Guardando'
    });
    load.present();
    const contactFormValue = await this.contactForm.value;
    const newContact: NewContact =  await {
      givenName: contactFormValue.name,
      phoneNumbers: [{number: contactFormValue.phone}]
    };
    await Contacts.saveContact(newContact).then(
      async () => {
        load.dismiss();
        this.router.navigate(['/home/view-cotization']);
        this.toastService.presentToast('Contacto guardado correctamente');
        const contactsList: Contact[] = await Contacts.getContacts().then(result => result.contacts);
        await this.dataBaseService.storeAllData(AuthConstants.CONTACTS, contactsList);
      }
    )
    .catch (
      (error: any) => {
        load.dismiss();
        this.alertController.create({
          header: 'Error',
          message: JSON.stringify(error)
        });
        this.toastService.presentToast('Error... ocurrió un problema');
      }
    );
  }

  closeModal(){
    this.modalController.dismiss();
  }
}
