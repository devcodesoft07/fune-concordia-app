import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddFuneralServiceQuoteComponent } from './add-funeral-service-quote.component';

describe('AddFuneralServiceQuoteComponent', () => {
  let component: AddFuneralServiceQuoteComponent;
  let fixture: ComponentFixture<AddFuneralServiceQuoteComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddFuneralServiceQuoteComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddFuneralServiceQuoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
