/* eslint-disable @typescript-eslint/no-inferrable-types */
/* eslint-disable @typescript-eslint/naming-convention */
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Contact, Contacts } from '@capacitor-community/contacts';
import { isPlatform, ModalController } from '@ionic/angular';
import { AuthConstants } from 'src/app/core/config/auth-constants.config';
import { IFuneralService } from 'src/app/core/models/funeral-service/funeralService.model';
import { IInitial } from 'src/app/core/models/initial.model';
import { ISector } from 'src/app/core/models/memorial-lot/sector.model';
import { IOption } from 'src/app/core/models/option.model';
import { IDataToQuote, IDiscountPrice, IRegularPrice } from 'src/app/core/models/quotes/quote.model';
import { DataBaseService } from 'src/app/core/service/data-base/data-base.service';
import { SendQuoteComponent } from '../send-quote/send-quote.component';

@Component({
  selector: 'app-add-funeral-service-quote',
  templateUrl: './add-funeral-service-quote.component.html',
  styleUrls: ['./add-funeral-service-quote.component.scss'],
})
export class AddFuneralServiceQuoteComponent implements OnInit {
  @Input() dataQuote: IDataToQuote<ISector | IFuneralService>;
  @Input() coinStatus: boolean;
  @Input() option: IOption;
  @Output() initialPayEventEmitter: EventEmitter<number> = new EventEmitter();
  @Output() totalValueDiscount: EventEmitter<number> = new EventEmitter();
  @Output() totalPriceFinal: EventEmitter<number> = new EventEmitter();
  @Output() changeObservationEventEmitter: EventEmitter<string> = new EventEmitter();
  initialPay: number;
  factorValue: number;
  totalValueDiscout: number;
  discountData: IDiscountPrice;
  initial: IInitial;
  observation: string = '';
  constructor(
    private modalController: ModalController,
    private dataBaseService: DataBaseService,
  ) { }

  ngOnInit() {
    this.dataBaseService.getData(AuthConstants.INITIALS)
    .subscribe(
      (res: IInitial) => {
        this.initial = res;
        if(this.dataQuote.title === 'Sectores'){
          this.factorValue = this.dataQuote.discount.lot_value;
        }else{
          this.factorValue = this.dataQuote.discount.fune_value;
        }
        let totalPrice: number = (this.dataQuote.data.predicted_price * 1);
        if (this.dataQuote.crematorium_funeral) {
          totalPrice += (this.option.fune_cremated * 1);
        }
        if (this.dataQuote.closed_funeral) {
          totalPrice -= (this.option.fune_closed * 1);
        }
        this.initialPay = Math.ceil(totalPrice * (this.initial.first / 100));
        this.initialPayEventEmitter.emit(this.initialPay);
      }
    );
  }

  async sendQuote(){
    const dataToSendWhat: IDataToQuote<ISector | IFuneralService> = {
      data: this.dataQuote.data,
      discount: this.dataQuote.discount,
      closed_funeral: this.dataQuote.closed_funeral,
      crematorium_funeral: this.dataQuote.crematorium_funeral,
      old_customer: this.dataQuote.old_customer,
      inv_initial: this.initialPay,
      title: this.dataQuote.title,
      observation: this.observation,
    };
    const modal = await this.modalController.create({
      component: SendQuoteComponent,
      cssClass: 'modal-form',
      componentProps: {
        data: dataToSendWhat,
        initial: this.initialPay
      },
    });
    await modal.present();

    await modal.onWillDismiss()
    .then(result => {

    });
  }
  async selectedService(event){
    this.initialPay = await event.detail.value;
    this.initialPayEventEmitter.emit(this.initialPay);
    // this.calculateCost();
  }

  changeDiscount(data: any): void {
    this.totalValueDiscout = data;
    this.totalValueDiscount.emit(data);
  }

  changeTotlaPrice(data: any): void {
    this.totalPriceFinal.emit(data);
  }

  changeObservation(): void {
    this.changeObservationEventEmitter.emit(this.observation);
  }

}
