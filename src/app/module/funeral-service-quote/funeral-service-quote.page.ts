/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/no-inferrable-types */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { NavController, ViewDidEnter } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { AnimationOptions } from '@ionic/angular/providers/nav-controller';
import { map } from 'rxjs/operators';
import { AuthConstants } from 'src/app/core/config/auth-constants.config';
import { IFuneralService } from 'src/app/core/models/funeral-service/funeralService.model';
import { ISector } from 'src/app/core/models/memorial-lot/sector.model';
import { IOption } from 'src/app/core/models/option.model';
import { IDataToQuote } from 'src/app/core/models/quotes/quote.model';
import { DataBaseService } from 'src/app/core/service/data-base/data-base.service';
import { DataServiceQuoteService } from 'src/app/core/service/data-service-cuote/data-service-quote.service';
import { ToastService } from 'src/app/core/service/toast/toast.service';
@Component({
  selector: 'app-funeral-service-quote',
  templateUrl: './funeral-service-quote.page.html',
  styleUrls: ['./funeral-service-quote.page.scss'],
})
export class FuneralServiceQuotePage implements OnInit, ViewDidEnter {
  coinStatus: boolean = true;
  funeralServiceId: number;
  dataQuote: IDataToQuote<ISector | IFuneralService>;
  initiaPay: number = 0;
  totalDiscount: number;
  totalPrice: number;
  observation: string = '';
  option: IOption;
  constructor(
    private activateRoute: ActivatedRoute,
    private dataServiceQuoteService: DataServiceQuoteService,
    private dataBaseService: DataBaseService,
    private navCtrl: NavController,
    private toastService: ToastService,
    private loadingController: LoadingController,
  ) {
  }


  ionViewDidEnter(): void {
    this.dataServiceQuoteService.dataToCuote$
    .pipe(
      map(
        (res) => {
          this.dataBaseService.getData(AuthConstants.OPTIONS)
          .subscribe(
            (option: IOption) => {
              this.option = option;
            }
          );
          return res;
        }
      )
    )
    .subscribe(
      async (res: IDataToQuote<ISector | IFuneralService>) => {
        if (!res.data) {
          this.dataBaseService.existKey(AuthConstants.DATATOCUOTE).subscribe(
            (isExist: boolean) =>{
              if (isExist) {
                let data: IDataToQuote<ISector | IFuneralService>;
                this.dataBaseService.getData(AuthConstants.DATATOCUOTE).subscribe(
                  (dataFromRes: any) => {
                    data = dataFromRes;
                    this.dataQuote = data;
                  }
                );
              }
            }
          );
        } else {
          this.dataQuote = res;
        }
      }
    )
    .unsubscribe();
  }
  ngOnInit(): void {

  }
  back(): void {
    const animations: AnimationOptions = {
      animated: true,
      animationDirection: 'back'
    };
    this.navCtrl.back(animations);
  }

  changeInitialPay(initialPay: number): void {
    this.initiaPay = initialPay;
  }
  changeTotalDiscount(totalDiscount: number): void {
    this.totalDiscount = totalDiscount;
  }
  changeTotalPrice(totalPrice: number): void {
    this.totalPrice = totalPrice;
  }
  changeCurrency(event: any): void {
    this.coinStatus = event.detail.checked;
  }
  changeObservation(observation: any): void {
    this.observation = observation;
  }

  async sendToCombo() {
    const load = await this.loadingController.create({
      message: 'Añadiendo al combo'
    });
    load.present();
    const dataToCombo: IDataToQuote<ISector|IFuneralService> =  {
      data: this.dataQuote.data,
      closed_funeral: this.dataQuote.closed_funeral,
      crematorium_funeral: this.dataQuote.crematorium_funeral,
      discount: this.dataQuote.discount,
      inv_initial: this.initiaPay,
      old_customer: true,
      title: this.dataQuote.title,
      totalDiscount: this.totalDiscount,
      totalPrice: this.totalPrice,
      observation: this.observation
    };
    this.dataQuote = dataToCombo;
    setTimeout(() => {
      this.dataServiceQuoteService.totalDiscount$.subscribe(
        (res: any) => {
          dataToCombo.totalDiscount = res;
          this.dataServiceQuoteService.addProduct(dataToCombo);
          load.dismiss();
        }
        ).unsubscribe();
      }, 1000);
    }
}
