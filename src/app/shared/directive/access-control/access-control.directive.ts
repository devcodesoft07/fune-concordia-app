import { Directive, ElementRef, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { AuthConstants } from 'src/app/core/config/auth-constants.config';
import { IAuthResponse } from 'src/app/core/models/login/auth-response.model';

@Directive({
  selector: '[appAccessControl]'
})
export class AccessControlDirective implements OnInit {

  constructor(
    private elementRef: ElementRef,
    private cookieService: CookieService
  ) { }

  async ngOnInit() {
    this.elementRef.nativeElement.style.display = await 'none';
    await this.checkAccess();
  }

  async checkAccess() {
    const data: IAuthResponse = await JSON.parse(this.cookieService.get(AuthConstants.AUTH));
    this.elementRef.nativeElement.style.display = await data.user.role_name === 'Administrador' ? 'block' : 'none';
  }
}
