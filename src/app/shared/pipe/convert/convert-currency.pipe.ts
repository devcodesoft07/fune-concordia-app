import { Pipe, PipeTransform } from '@angular/core';
import { AuthConstants } from 'src/app/core/config/auth-constants.config';
import { DataBaseService } from 'src/app/core/service/data-base/data-base.service';

@Pipe({
  name: 'convertCurrency'
})
export class ConvertCurrencyPipe implements PipeTransform {
  currency: any;

  constructor(
    private dataBaseService: DataBaseService
  ) {
    this.dataBaseService.getData(AuthConstants.OPTIONS)
    .subscribe(
      (res: any) => {
        this.currency = res.currency;
      }
    );
  }

  transform(value: number, isConvert: boolean): number {
    if (!isConvert) {
      return Math.ceil(value * this.currency);
    } else {
      return Math.ceil(value);
    }
  }

}
