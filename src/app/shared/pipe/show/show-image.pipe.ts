import { Pipe, PipeTransform } from '@angular/core';
import { IImage } from 'src/app/core/models/memorial-lot/image.model';

@Pipe({
  name: 'showImage'
})
export class ShowImagePipe implements PipeTransform {

  transform(images: IImage[]): boolean {
    const filterFound =  images.filter(
      (data: IImage) => data.image.toLowerCase().includes('empty') || data.image === ''
    );
    return filterFound.length !== 0;
  }

}
