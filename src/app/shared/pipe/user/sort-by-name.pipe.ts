import { Pipe, PipeTransform } from '@angular/core';
import { User } from 'src/app/core/models/user/user.model';

@Pipe({
  name: 'sortByName',
  pure: false
})
export class SortByNamePipe implements PipeTransform {

  transform(users: User[]): User[] {
    return users.sort((prevUser, currentUser) => {
      if(prevUser.name < currentUser.name) { return -1; }
      if(prevUser.name > currentUser.name) { return 1; }
      return 0;
    });
  }

}
