import { Pipe, PipeTransform } from '@angular/core';
import { User } from 'src/app/core/models/user/user.model';

@Pipe({
  name: 'searchByNane',
  pure: false
})
export class SearchByNanePipe implements PipeTransform {

  transform(users: User[], text: string): User[] {
    const textToSearch = text.toLowerCase();
    if (textToSearch !== '') {
      return users.filter(user => user.name.toLowerCase().includes(textToSearch));
    }
    return users;
  }

}
