import { Pipe, PipeTransform } from '@angular/core';
import { Contact, PhoneNumber } from '@capacitor-community/contacts';

@Pipe({
  name: 'findNumber'
})
export class FindNumberPipe implements PipeTransform {

  transform(contacts: Contact[], numberToSearch: string): Contact[] {
    const text = numberToSearch.toLowerCase();
    if (text === '') {
      return contacts;
    } else {
      return contacts.filter(
        (contact: Contact) => contact.phoneNumbers.filter((phone: PhoneNumber) => phone.number.includes(text))
      );
    }
  }

}
