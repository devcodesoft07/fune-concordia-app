import { Component, DoCheck, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { LoadingController, MenuController } from '@ionic/angular';
import { AuthConstants } from 'src/app/core/config/auth-constants.config';
import { StorageService } from 'src/app/core/service/storage/storage.service';
import { AuthService } from 'src/app/core/service/auth/auth.service';
import { Router } from '@angular/router';
import { IAuthResponse, IUser } from 'src/app/core/models/login/auth-response.model';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, DoCheck {
  @Input() title = 'Lote Memorial';
  user: IUser;
  constructor(
    private menu: MenuController,
    private authService: AuthService,
    private cookieService: CookieService,
    private router: Router,
    private loadingController: LoadingController
  ) {
  }

  ngDoCheck(): void {
    this.verifyUser();
  }

  verifyUser() {
    const existAuthUser = this.cookieService.check(AuthConstants.AUTH);
    if (existAuthUser) {
      const data: IAuthResponse = JSON.parse(this.cookieService.get(AuthConstants.AUTH));
      this.user = data.user;
    }
  }

  ngOnInit(): void {
  }

  async openMenuNav() {
    // console.log('abrir menu bar');
    // this.menu.enable(true, 'main');
    await this.menu.open('main');
  }

  async logout() {
    const loading = await this.loadingController.create({
      message: 'Cerrando sesión...',
      duration: 2000
    });
    await loading.present();
    this.authService.logout()
    .subscribe(
      async (res: any) => {
        this.cookieService.deleteAll();
        this.router.navigate(['/login']);
        await loading.dismiss();
      }
    );
  }
}
