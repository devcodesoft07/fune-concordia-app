/* eslint-disable @typescript-eslint/member-ordering */
import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { Filesystem, Directory } from '@capacitor/filesystem';
import { LoadingController, Platform, ToastController } from '@ionic/angular';
import { Camera, CameraResultType, CameraSource, Photo } from '@capacitor/camera';
import { finalize } from 'rxjs/operators';

import { AngularFireStorage } from '@angular/fire/compat/storage';
import { Observable } from 'rxjs';
import { User } from 'src/app/core/models/user/user.model';

const IMAGE_DIR = 'stored-images';
interface LocalFile {
  name: string;
  path: string;
  data: string;
}
@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss'],
})
export class FileUploadComponent implements OnInit {
  @Input() userName: User;
  @Input() category: string;
  images: LocalFile[] = [];
  slideOpts = {
    initialSlide: 1,
    speed: 400
  };

  uploadPercent: Observable<number>;
  downloadURL: Observable<string>;
  isUpload: boolean;

  constructor(
    private modalController: ModalController,
    private plt: Platform,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private storage: AngularFireStorage,
  ) { }

  async ngOnInit() {
    this.loadFiles();
    console.log('USr', this.userName);
  }

  async loadFiles() {
    this.images = [];

    const loading = await this.loadingCtrl.create({
      message: 'Loading data...',
    });
    await loading.present();

    Filesystem.readdir({
      path: IMAGE_DIR,
      directory: Directory.Data,
    }).then(result => {
      this.loadFileData(result.files);
    },
      async (err) => {
        // Folder does not yet exists!
        await Filesystem.mkdir({
          path: IMAGE_DIR,
          directory: Directory.Data,
        });
      }
    ).then(_ => {
      loading.dismiss();
    });
  }

  // Get the actual base64 data of an image
  // base on the name of the file
  async loadFileData(fileNames: string[]) {
    for (const f of fileNames) {
      const filePath = `${IMAGE_DIR}/${f}`;

      const readFile = await Filesystem.readFile({
        path: filePath,
        directory: Directory.Data,
      });

      this.images.push({
        name: f,
        path: filePath,
        data: `data:image/jpeg;base64,${readFile.data}`,
      });
    }
  }
  // Little helper
  async presentToast(text) {
    const toast = await this.toastCtrl.create({
      message: text,
      duration: 3000,
    });
    toast.present();
  }

  async selectImage() {
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Base64,
      source: CameraSource.Prompt // Camera, Photos or Prompt!
    });

    if (image) {
      this.saveImage(image);
    }
  }
  // Create a new file from a capture image
  async saveImage(photo: Photo) {
    const base64Data = await this.readAsBase64(photo);

    const fileName = new Date().getTime() + '.jpg';
    const savedFile = await Filesystem.writeFile({
        path: `${IMAGE_DIR}/${fileName}`,
        data: base64Data,
        directory: Directory.Data
    });

    // Reload the file list
    // Improve by only loading for the new image and unshifting array!
    this.loadFiles();
  }

  // https://ionicframework.com/docs/angular/your-first-app/3-saving-photos
  private async readAsBase64(photo: Photo) {
    if (this.plt.is('hybrid')) {
      const file = await Filesystem.readFile({
          path: photo.path
      });

      return file.data;
    }
    else {
      // Fetch the photo, read as a blob, then convert to base64 format
      const response = await fetch(photo.webPath);
      const blob = await response.blob();

      return await this.convertBlobToBase64(blob) as string;
    }
  }
  // Helper function
  convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onerror = reject;
    reader.onload = () => {
        resolve(reader.result);
    };
    reader.readAsDataURL(blob);
  });

  async startUpload(file: LocalFile) {
    const response = await fetch(file.data);
    const blob = await response.blob();
    const fileData = new File([blob], file.name, {type: 'image'});
    this.uploadFile(fileData, file);
  }

  async uploadFile(fileData: File, file: LocalFile) {
    this.isUpload = true;
    // const filePath = `${this.category}/${this.partner.id} - ${this.partner.name} - ${this.partner.last_name}/${fileData.name}`;
    const filePath = `${this.category}/${this.userName}/${fileData.name}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, fileData);
    const loading = await this.loadingCtrl.create({
      message: 'Subiendo Imagen...',
    });
    await loading.present();
    // observe percentage changes
    this.uploadPercent = task.percentageChanges();
    // get notified when the download URL is available
    task.snapshotChanges().pipe(
        finalize(async () => {
          this.downloadURL = fileRef.getDownloadURL();
          this.isUpload = false;
          loading.dismiss();
          this.presentToast('Imagen subida correctamente.');
          await this.deleteImage(file, false);
          await this.sendUrlVoucher();
        })
     )
    .subscribe();
  }

  async fileUpload(event: any) {
    const file = event.target.files[0];
    this.isUpload = true;
    // const filePath = `${this.category}/${this.partner.id} - ${this.partner.name} - ${this.partner.last_name}/${fileData.name}`;
    const filePath = `${this.category}/${this.userName}/${file.name}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
    const loading = await this.loadingCtrl.create({
      message: 'Subiendo Imagen...',
    });
    await loading.present();
    // observe percentage changes
    this.uploadPercent = task.percentageChanges();
    // get notified when the download URL is available
    task.snapshotChanges().pipe(
        finalize(async () => {
          this.downloadURL = fileRef.getDownloadURL();
          this.isUpload = false;
          loading.dismiss();
          this.presentToast('Imagen subida correctamente.');
          await this.sendUrlVoucher();
        })
     )
    .subscribe();
  }

  sendUrlVoucher() {
    this.downloadURL.subscribe(
      (res: string) => {
        this.modalController.dismiss({
          voucher: res
        });
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  async deleteImage(file: LocalFile, option: boolean) {
    await Filesystem.deleteFile({
      directory: Directory.Data,
      path: file.path
    });
    if (option) {
      if (this.downloadURL) {
        this.downloadURL.subscribe(
          (res: string) => {
            // console.log(res);
            if (res !== undefined || res !== '') {
              this.storage.refFromURL(res).delete();
            }
            this.presentToast('Imagen eliminada.');
          },
          (error: any) => {
            console.log(error);
          }
        );
      } else {
        console.log('No hacer nada');
      }
    }
    this.loadFiles();
  }

  dismiss(): void {
    this.modalController.dismiss();
  }
}
