import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { AvatarModule } from 'ngx-avatar';
//Components
import { HeaderComponent } from './components/header/header.component';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { SkeletonCreenComponent } from './components/skeleton-creen/skeleton-creen.component';
import { BuildingComponent } from './components/building/building.component';
//Directives
import { RouterActiveDirective } from './directive/router-active/router-active.directive';

//Angular Material
import { AngularMaterialModule } from './angular-material/angular-material.module';
import { UnauthorizedComponent } from './components/unauthorized/unauthorized.component';
import { AccessControlDirective } from './directive/access-control/access-control.directive';
import { FindNumberPipe } from './pipe/findNumber/find-number.pipe';
import { ShowImagePipe } from './pipe/show/show-image.pipe';
import { ConvertCurrencyPipe } from './pipe/convert/convert-currency.pipe';
import { SearchByNanePipe } from './pipe/user/search-by-nane.pipe';
import { SortByNamePipe } from './pipe/user/sort-by-name.pipe';

@NgModule({
  declarations: [
    HeaderComponent,
    RouterActiveDirective,
    FileUploadComponent,
    SkeletonCreenComponent,
    BuildingComponent,
    UnauthorizedComponent,
    AccessControlDirective,
    FindNumberPipe,
    ShowImagePipe,
    ConvertCurrencyPipe,
    SearchByNanePipe,
    SortByNamePipe,
 ],
  imports: [
    CommonModule,
    IonicModule,
    AvatarModule,
    AngularMaterialModule
  ],
  exports: [
    HeaderComponent,
    RouterActiveDirective,
    FileUploadComponent,
    SkeletonCreenComponent,
    BuildingComponent,
    UnauthorizedComponent,
    AccessControlDirective,
    FindNumberPipe,
    ShowImagePipe,
    ConvertCurrencyPipe,
    SearchByNanePipe,
    SortByNamePipe,
  ]
})
export class SharedModule { }
