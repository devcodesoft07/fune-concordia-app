import { Injectable } from '@angular/core';
import { CanActivate, Router, UrlTree } from '@angular/router';
import { AuthConstants } from 'src/app/core/config/auth-constants.config';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private cookieService: CookieService,
    private router: Router){}
    canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree{
      const existToken = this.cookieService.check(AuthConstants.AUTH);

      if (!existToken) {
        return this.router.navigate(['/login']).then(() => false);
      }
      return true;
    }
}
