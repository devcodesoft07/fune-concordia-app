import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../core/service/auth/auth.service';
import { StorageService } from '../core/service/storage/storage.service';
import { ToastService } from '../core/service/toast/toast.service';
import { Router } from '@angular/router';
import { AuthConstants } from '../core/config/auth-constants.config';
import { PatternEnum } from '../core/enum/pattern-enum';
import { CookieService } from 'ngx-cookie-service';
import { DataBaseService } from '../core/service/data-base/data-base.service';
import { DiscountService } from '../core/service/discount/discount.service';
import { IPaginator } from '../core/models/paginator.model';
import { IDiscount } from '../core/models/discount/discount-model';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  showPassword: boolean;
  passwordToggleIcon: string;
  formLogin: FormGroup;
  errorMessages;
  showLoader: boolean;
  state: boolean;
  isChecked: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private storage: StorageService,
    private toast: ToastService,
    private cookieService: CookieService,
    private dataBaseService: DataBaseService,
    private discountService: DiscountService,
		private router: Router) {
    this.showPassword = false;
    this.passwordToggleIcon = 'eye-off';
    this.showLoader = false;
    this.errorMessages = this.createErrorMessages();
    this.buildForm();
  }

  ngOnInit() {
    this.getCredentialsStorage();
  }

  createErrorMessages() {
    return {
      email: [
        { type: 'required', message: 'El correo electronico es obligatorio.' },
        { type: 'email', message: 'Debe ingresar un correo electrónico con el siguiente formato: elemplo@ejemplo.com.'},
        { type: 'minlength', message: 'Debe introducir como mínimo 15 caracteres.'},
        { type: 'maxlength', message: 'Debe introducir como máximo 50 caracteres.'},
        { type: 'pattern', message: 'Introduzca un correo electronico valido' },
      ],
      password: [
        { type: 'required', message: 'La contraseña es obligatoria.' },
        { type: 'minlength', message: 'Debe introducir como mínimo 8 caracteres.'},
        { type: 'maxlength', message: 'Debe introducir como máximo 25 caracteres.'},
      ],
    };
  }
  getForm() {
    return this.formLogin;
  }

  getEmail() {
    return this.formLogin.get('email');
  }

  getPassword() {
    return this.formLogin.get('password');
  }

  buildForm() {
    this.formLogin  = this.formBuilder.group({
      email: ['', [
        Validators.required,
        Validators.email,
        Validators.minLength(15),
        Validators.maxLength(50),
        Validators.pattern(PatternEnum.EMAIL_PATTERN),
      ]],
      password:['', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(25)
      ]],
    });
  }

  getCredentialsStorage(){
    this.storage.get(AuthConstants.CREDENTIAL)
    .then((res)=>{
      if(res){
        this.isChecked = true;
        this.formLogin.patchValue(res);
      }
    }).catch(
      (error: any) => {
        this.isChecked = false;
        this.formLogin.reset();
      }
    );
  }


  togglePassword(): void {
    this.showPassword = !this.showPassword;
    this.passwordToggleIcon = this.passwordToggleIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  checkboxClick(e){
    if(e.target.checked){
      const credentials = this.formLogin.getRawValue();
      this.storage.store(AuthConstants.CREDENTIAL, credentials);
    }else{
      this.storage.removeStorageItem(AuthConstants.CREDENTIAL);
    }
  }
  loginUser(){
    this.showLoader = true;
    const credentials = this.formLogin.getRawValue();
    this.auth.login(credentials).subscribe((res) => {
      if(res){
        this.showLoader = false;
        this.cookieService.set(AuthConstants.AUTH, JSON.stringify(res));
        this.router.navigateByUrl('/', {skipLocationChange: true})
        .then(
          () => {
            this.dataBaseService.existKey(AuthConstants.DISCOUNTS).subscribe(
              (isExist: boolean) => {
                if (!(isExist)) {
                  this.getDiscounts();
                }
              }
            );
            this.router.navigate(['home']);
          }
        );
        this.toast.presentToast('Acceso autorizado');
      }
    },(error)=>{
      this.showLoader = false;
      this.toast.presentToast(error.error.message);
    });
  }

  getDiscounts(): void {
    this.discountService.getAllDiscount().subscribe(
      (res: IPaginator<IDiscount[]>) => {
        this.dataBaseService.storeAllData(AuthConstants.DISCOUNTS, res.data);
      },
      (error: any) => {
        console.log(error);
      }
    );
  }
}
