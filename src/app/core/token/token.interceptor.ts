/* eslint-disable @typescript-eslint/naming-convention */
import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';

import { catchError } from 'rxjs/operators';
import { StorageService } from '../service/storage/storage.service';
import { AuthConstants } from '../config/auth-constants.config';
import { CookieService } from 'ngx-cookie-service';
import { IAuthResponse } from '../models/login/auth-response.model';

@Injectable({ providedIn: 'root' })
export class InterceptorToken implements HttpInterceptor {
  token: string;

  constructor(
    private router: Router,
    private localStorageService: StorageService,
    private cookieService: CookieService
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const existToken = this.cookieService.check(AuthConstants.AUTH);
    let re = req;
    if (existToken) {
      const data: IAuthResponse = JSON.parse(this.cookieService.get(AuthConstants.AUTH));
      re = req.clone({
        setHeaders: {
          'Access-Control-Allow-Headers': 'Origin',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + data.access_token,
        },
      });
    }
    return next.handle(re)
      .pipe(
        catchError(
          (error) => {
            console.warn(error);
            if(error === 'Token has expired' || error === 'Unauthenticated.'){
              this.localStorageService.clear();
              this.router.navigate(['/']);
            }
            return throwError(error);
          }
        )
      );
  }
}
