/* eslint-disable @typescript-eslint/naming-convention */
export interface ISector{
  id?: number;
  name: string;
  real_price: number;
  discount_price: number;
  predicted_price: number;
  place_id: number;
  images: IImageItem[];
}

export interface IImageItem {
  id: number;
  image: string;
}

export interface ISectorSave{
  name: string;
  real_price: number;
  discount_price: number;
  predicted_price: number;
  place_id: number;
  image: string[];
}
