/* eslint-disable @typescript-eslint/naming-convention */
export interface IMemorialLot{
  id?: number;
  name: string;
  image: string;
  sectors_total: number;
}
