/* eslint-disable @typescript-eslint/naming-convention */
export interface User{
  id: number;
  name: string;
  email: string;
  photo: string;
  role_id: number;
  status: string;
}
