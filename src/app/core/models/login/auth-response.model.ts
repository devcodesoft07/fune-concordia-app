/* eslint-disable @typescript-eslint/naming-convention */
export interface IAuthResponse {
    access_token: string;
    token_type: string;
    expires_at: string;
    user: IUser;
}


export interface IUser {
  email: string;
  name: string;
  photo: string;
  role_id: number;
  role_name: string;
  status: string;
}
