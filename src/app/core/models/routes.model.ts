export interface IRoutes {
  route: string;
  label: string;
  icon: string;
}
