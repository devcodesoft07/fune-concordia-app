/* eslint-disable @typescript-eslint/naming-convention */
export interface IOption {
  id: number;
  fune_closed: number;
  fune_cremated: number;
  old_customer: number;
  currency: number;
}
