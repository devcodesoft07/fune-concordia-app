/* eslint-disable @typescript-eslint/naming-convention */
import { IDiscount } from 'src/app/core/models/discount/discount-model';
import { ISector } from '../memorial-lot/sector.model';
import { IFuneralService } from './../funeral-service/funeralService.model';
export interface IDataToQuote<T>{
  data?: T;
  discount?: IDiscount;
  closed_funeral?: boolean;
  crematorium_funeral?: boolean;
  old_customer?: boolean;
  inv_initial?: number;
  title?: string;
  totalDiscount?: number;
  totalPrice?: number;
  observation?: string;
}

export interface IQuotaServer extends IDataToQuote<ISector | IFuneralService>{
  initial: number;
}

export interface IRegularPrice {
  emergency_price?: number;
  money_save?: number;
  initial_quote?: number;
  second_intial_quote?: number;
  third_initial_quote?: number;
  total_cash?: number;
  totalPrice?: number;
}

export interface IFinalPrice {
  final_balanced?: number;
  total_discount?: number;
  initial_quote?: number;
  anual_interest?: number;
  monthly_fee?: number;
}

export interface IDiscountPrice {
  combo_discount?: number;
  old_customer_discount?: number;
  term_discount?: number;
  initial_quote_discount?: number;
  total_discount?: number;
}
