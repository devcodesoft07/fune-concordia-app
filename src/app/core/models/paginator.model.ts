/* eslint-disable @typescript-eslint/naming-convention */
export interface IPaginator<T> {
  data: T;
  links: ILinksImportant;
  meta: IMeta;
  users?: ITotalUsers;
}

interface ILinksImportant {
  first: string;
  last: string;
  prev: string | null;
  next: string;
}

interface IMeta {
  current_page: number;
  from: number;
  last_page: number;
  per_page: number;
  links: ILinks[];
  to: number;
  total: number;
}

interface ILinks {
  url: string | null;
  label: string;
  active: boolean;
}

export interface ITotalUsers{
  quantity_total: number;
  quantity_active: number;
  quantity_inactive: number;
}

