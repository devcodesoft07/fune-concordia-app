import { IPaginator } from '../paginator.model';

/* eslint-disable @typescript-eslint/naming-convention */
export interface IDiscount {
  id?: number;
  term: number;
  lot_percentage: number;
  lot_value: number;
  fune_percentage: number;
  fune_value: number;
  initial_6: number;
  initial_10: number;
  initial_20: number;
  cash: number;
}
