export interface IServerResponse<T> {
  data: T;
  message: string;
}


export interface IServerResponseOnlyMessage {
  message: string;
}
