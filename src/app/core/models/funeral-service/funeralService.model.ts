import { IImage } from '../memorial-lot/image.model';

/* eslint-disable @typescript-eslint/naming-convention */
export interface IFuneralService {
    id?: number;
    name: string;
    real_price: number;
    discount_price: number;
    predicted_price: number;
    images?: IImage[];
}

export interface IFuneralServiceSave  {
  name: string;
  real_price: number;
  discount_price: number;
  predicted_price: number;
  image?: string[];
}
