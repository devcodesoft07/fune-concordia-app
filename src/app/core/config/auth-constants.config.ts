/* eslint-disable @typescript-eslint/naming-convention */
export class AuthConstants {
    public static readonly AUTH = 'USER_INFO';
    public static readonly CREDENTIAL = 'CREDENTIAL';
    public static readonly DISCOUNTS = 'discounts';
    public static readonly DISCOUNTS_FUNE = 'discounts_fune';
    public static readonly INITIALS = 'initials';
    public static readonly OPTIONS = 'options';
    public static readonly SECTORS = 'sectors';
    public static readonly SERVICES = 'services';
    public static readonly PLACES = 'places';
    public static readonly DATATOCUOTE = 'data_to_cuote';
    public static readonly CONTACTS = 'contacts';
    public static readonly CURRENCY = 'currency';
}
