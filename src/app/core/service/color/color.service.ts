import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ColorService {

  constructor() { }

  changeBgMatCell(option: string, index: number): string {

    if ((index % 2) !== 0) {
      if (option === 'interest') {
        return '#E5F6DA';
      } else {
        return '#FFDAD4';
      }
    }
  }
}
