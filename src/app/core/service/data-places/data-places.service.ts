import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataPlacesService {
  dataPlace: any = {};

  constructor() { }

  saveData(data: any){
    this.dataPlace = data;
  }

  getData(){
    return this.dataPlace;
  }
}
