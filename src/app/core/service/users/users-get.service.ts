/* eslint-disable arrow-body-style */
import { map } from 'rxjs/operators';
import { HttpService } from '../http/http.service';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../../models/user/user.model';
import { IPaginator } from '../../models/paginator.model';
import { IServerResponse } from '../../models/server-response.model';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersGetService {
  public usersObserver$: Observable<User[]>;
  private users: User[];
  private objectSourceList = new BehaviorSubject<User[]>([]);
  constructor(
  ) {
    this. users = [];
    this.usersObserver$ = this.objectSourceList.asObservable();
  }

  addUsers(users: User[]): void {
    this.users = [...this.users, ...users];
    this.objectSourceList.next(this.users);
  }

  clearDataUsers(): void {
    this.users = [];
    this.objectSourceList.next(this.users);
  }

}
