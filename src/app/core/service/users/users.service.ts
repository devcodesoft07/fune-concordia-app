/* eslint-disable arrow-body-style */
import { map } from 'rxjs/operators';
import { HttpService } from './../http/http.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../../models/user/user.model';
import { IPaginator } from '../../models/paginator.model';
import { IServerResponse } from '../../models/server-response.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  url: string = environment.apiUrl;
  constructor(
    private httpService: HttpService,
    private httpClient: HttpClient,
  ) { }

  getAllUsers(page: number = 1): Observable<IPaginator<User[]>> {
    let params: HttpParams = new HttpParams();
    params = params.append('page', page);
    return this.httpClient.get(this.url + 'users', { params })
      .pipe(
        map(
          (res: IPaginator<User[]>) => res as IPaginator<User[]>
        )
      );
  }

  getAllUserById(id: number): Observable<User> {
    return this.httpService.get('users/' + id)
      .pipe(
        map(
          (res: User) => res as User
        )
      );
  }

  createUser(data: User): Observable<User> {
    return this.httpService.post('users', data)
      .pipe(
        map(
          (res: IServerResponse<User>) => res.data as User
        )
      );
  }

  deleteUser(id: number): Observable<boolean> {
    return this.httpService.delete('users/' + id)
      .pipe(
        map(
          (res: string) => {
            return true;
          }
        )
      );
  }

  updateUser(data: User): Observable<User> {
    return this.httpService.put('users', data)
      .pipe(
        map(
          (res: IServerResponse<User>) => res.data as User
        )
      );
  }
}
