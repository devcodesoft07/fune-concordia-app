import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(
    private alertController: AlertController
  ) { }

  async presentAlert(headerData?: string, subHeaderData?: string, messageData?: string) {
    const alert = await this.alertController.create({
      header: headerData? headerData : 'Alert',
      mode: 'md',
      subHeader: subHeaderData? subHeaderData : 'Subtitle',
      message: messageData? messageData : 'This is an alert message.',
      buttons: ['OK']
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    // console.log('onDidDismiss resolved with role', role);
  }

}
