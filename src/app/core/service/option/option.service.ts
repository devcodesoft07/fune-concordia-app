import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IOption } from '../../models/option.model';
import { IPaginator } from '../../models/paginator.model';
import { IServerResponse } from '../../models/server-response.model';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class OptionService {

  constructor(
    private httpService: HttpService
  ) { }

  getAllOptions(): Observable<IPaginator<IOption[]>>{
    return this.httpService.get('options')
    .pipe(
      map(
        (res: IPaginator<IOption[]>) => res as IPaginator<IOption[]>
      )
    );
  }

  addOption( data: IOption): Observable<IOption>{
    return this.httpService.post('options', data)
    .pipe(
      map(
        (res: IPaginator<IOption>) => res.data as IOption
      )
    );
  }

  getOption(id: number): Observable<IOption>{
    return this.httpService.get('options/'+id)
    .pipe(
      map(
        (res: IServerResponse<IOption>) => res.data as IOption
      )
    );
  }

  updateOption(data: IOption): Observable<IOption>{
    return this.httpService.put('options',data)
    .pipe(
      map(
        (res: IServerResponse<IOption>) => res.data as IOption
      )
    );
  }


}
