import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IDiscount } from '../../models/discount/discount-model';
import { IPaginator } from '../../models/paginator.model';
import { IServerResponse, IServerResponseOnlyMessage } from '../../models/server-response.model';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class DiscountService {
  private endPoint!: string;
  constructor(private httpService: HttpService) { }

  getAllDiscount(isLote: boolean = true): Observable<IPaginator<IDiscount[]>> {
    this.endPoint = isLote? 'discounts' : 'discounts_fune';
    return this.httpService.get(this.endPoint)
    .pipe(
      map(
        (res: IPaginator<IDiscount[]>) => res as IPaginator<IDiscount[]>
      )
    );
  }

  addDiscount(data: IDiscount, isLote: boolean = true): Observable<IDiscount> {
    this.endPoint = isLote? 'discounts' : 'discounts_fune';
    return this.httpService.post( this.endPoint, data)
    .pipe(
      map(
        (res: IServerResponse<IDiscount>) => res.data as IDiscount
      )
    );
  }

  updateDiscount(data: IDiscount, isLote: boolean = true): Observable<IDiscount> {
    this.endPoint = isLote? 'discounts' : 'discounts_fune';
    return this.httpService.put(this.endPoint, data)
    .pipe(
      map(
        (res: IServerResponse<IDiscount>) => res.data as IDiscount
      )
    );
  }

  deleteDiscount(data: IDiscount, isLote: boolean = true): Observable<IServerResponseOnlyMessage> {
    this.endPoint = isLote? 'discounts' : 'discounts_fune';
    return this.httpService.delete(this.endPoint + '/' + data.id)
    .pipe(
      map(
        (res: IServerResponseOnlyMessage) => res as IServerResponseOnlyMessage
      )
    );
  }

}
