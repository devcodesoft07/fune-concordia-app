/* eslint-disable @typescript-eslint/naming-convention */
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IFuneralService } from '../../models/funeral-service/funeralService.model';
import { ISector } from '../../models/memorial-lot/sector.model';
import { IDataToQuote, IQuotaServer } from '../../models/quotes/quote.model';

@Injectable({
  providedIn: 'root'
})
export class GenerateQuotaService {
  apiUrl: string = environment.apiUrl;
  constructor(
    private httpClient: HttpClient
  ) { }

  getDataQuota(data: IDataToQuote<ISector|IFuneralService>[]): Observable<any> {
    const formControl: FormControl = new FormControl();
    formControl.setValue(data);
    return this.httpClient.post(this.apiUrl + 'places/quote', formControl.value);
  }
}
