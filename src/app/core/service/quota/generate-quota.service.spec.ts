import { TestBed } from '@angular/core/testing';

import { GenerateQuotaService } from './generate-quota.service';

describe('GenerateQuotaService', () => {
  let service: GenerateQuotaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GenerateQuotaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
