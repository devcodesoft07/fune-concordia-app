import { Injectable } from '@angular/core';
import { IAuth } from '../../models/login/auth.model';
import { Observable } from 'rxjs';
import { HttpService } from '../http/http.service';
import { AuthConstants } from '../../config/auth-constants.config';
import { StorageService } from '../storage/storage.service';
import { Router } from '@angular/router';
import { IAuthResponse } from '../../models/login/auth-response.model';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(
    private httpService: HttpService,
    private storageService: StorageService,
    private router: Router) { }

  login(credentials: IAuth): Observable<IAuthResponse> {
    return this.httpService.post('login', credentials)
    .pipe(
      map(
        (res: IAuthResponse) => res as IAuthResponse
        )
    );
	}

  logout(): Observable<IAuthResponse> {
    return this.httpService.get('logout')
    .pipe(
      map(
        (res: IAuthResponse) => res as IAuthResponse
        )
    );
  }
}
