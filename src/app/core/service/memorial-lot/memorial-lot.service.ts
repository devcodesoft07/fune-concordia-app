import { IServerResponse, IServerResponseOnlyMessage } from './../../models/server-response.model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IMemorialLot } from '../../models/memorial-lot/memorial-lot.model';
import { IPaginator } from '../../models/paginator.model';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class MemorialLotService {

  constructor(
    private httpService: HttpService
  ) { }

  getAllPlaces(): Observable<IPaginator<IMemorialLot[]>>{
    return this.httpService.get('places')
    .pipe(
      map(
        (res: IPaginator<IMemorialLot[]>) => res as IPaginator<IMemorialLot[]>
      )
    );
  }

  addPlace( data: IMemorialLot): Observable<IMemorialLot>{
    return this.httpService.post('places', data)
    .pipe(
      map(
        (res: IPaginator<IMemorialLot>) => res.data as IMemorialLot
      )
    );
  }

  getPlace(id: number): Observable<IMemorialLot>{
    return this.httpService.get('places/'+id)
    .pipe(
      map(
        (res: IServerResponse<IMemorialLot>) => res.data as IMemorialLot
      )
    );
  }

  updatePlace(data: IMemorialLot): Observable<IMemorialLot>{
    return this.httpService.put('places',data)
    .pipe(
      map(
        (res: IServerResponse<IMemorialLot>) => res.data as IMemorialLot
      )
    );
  }

  deleteMemorialLot(id: number): Observable<IServerResponseOnlyMessage>{
    return this.httpService.delete('places/'+ id)
    .pipe(
      map(
        (res: IServerResponseOnlyMessage) => res as IServerResponseOnlyMessage
      )
    );
  }

}
