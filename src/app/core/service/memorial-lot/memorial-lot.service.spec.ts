import { TestBed } from '@angular/core/testing';

import { MemorialLotService } from './memorial-lot.service';

describe('MemorialLotService', () => {
  let service: MemorialLotService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MemorialLotService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
