import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IFuneralService, IFuneralServiceSave } from '../../models/funeral-service/funeralService.model';
import { IPaginator } from '../../models/paginator.model';
import { HttpService } from '../http/http.service';
import { IServerResponse, IServerResponseOnlyMessage } from '../../models/server-response.model';
import { IImage } from '../../models/memorial-lot/image.model';


@Injectable({
  providedIn: 'root'
})
export class FuneralService {

  constructor(private httpService: HttpService) { }

  getAllFuneralService(): Observable<IPaginator<IFuneralService[]>>{
    return this.httpService.get('services')
    .pipe(
      map(
        (res: IPaginator<IFuneralService[]>) => res as IPaginator<IFuneralService[]>
      )
    );
  }

  addFuneralService(data: IFuneralServiceSave): Observable<IFuneralService> {
    return this.httpService.post('services', data)
    .pipe(
      map(
        (res: IServerResponse<IFuneralService>) => res.data as IFuneralService
      )
    );
  }

  updateFuneralService(data: IFuneralService): Observable<IFuneralService> {
    return this.httpService.put('services', data)
    .pipe(
      map(
        (res: IServerResponse<IFuneralService>) => res.data as IFuneralService
      )
    );
  }

  deleteFuneralService(data: IFuneralService): Observable<IServerResponseOnlyMessage> {
    return this.httpService.delete('services/'+ data.id)
    .pipe(
      map(
        (res: IServerResponseOnlyMessage) => res as IServerResponseOnlyMessage
      )
    );
  }

  getAllImagesOfServices(id: number): Observable<IServerResponse<IImage[]>>{
    return this.httpService.get('services/'+`${id}`+'/images')
    .pipe(
      map(
        (res: IServerResponse<IImage[]>) => res as IServerResponse<IImage[]>
      )
    );
  }

  updateImageOfFuneralService(idService: number, data: IImage): Observable<IImage>{
    return this.httpService.put('services/'+`${idService}`+'/images', data)
    .pipe(
      map(
        (res: IServerResponse<IImage>) => res.data as IImage
      )
    );
  }
}
