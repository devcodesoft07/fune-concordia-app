/* eslint-disable arrow-body-style */
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IPaginator } from '../../models/paginator.model';
import { ISector, ISectorSave } from '../../models/memorial-lot/sector.model';
import { HttpService } from '../http/http.service';
import { IServerResponse } from '../../models/server-response.model';
import { IImage } from '../../models/memorial-lot/image.model';

@Injectable({
  providedIn: 'root'
})
export class SectorService {

  constructor(
    private httpService: HttpService
  ) { }

  getAllSectorsOfPlace(idPlace: number): Observable<IPaginator<ISector[]>>{
    return this.httpService.get('places/'+`${idPlace}`+'/sectors')
    .pipe(
      map(
        (res: IPaginator<ISector[]>) => res as IPaginator<ISector[]>
      )
    );
  }

  addSectorForPlace(data: ISectorSave): Observable<ISector>{
    return this.httpService.post('sectors',data)
    .pipe(
      map(
        (res: IServerResponse<ISector>) => res.data as ISector
      )
    );
  }

  deleteSector(id: number): Observable<boolean>{
    return this.httpService.delete('sectors/'+ id)
    .pipe(
      map(
        (res: string) => {
          return true;
        }
      )
    );
  }

  updateSectorOfPlace(data: ISector): Observable<ISector>{
    return this.httpService.put('sectors',data)
    .pipe(
      map(
        (res: IServerResponse<ISector>) => res.data as ISector
      )
    );
  }

  getAllImagesOfSector(id: number): Observable<IServerResponse<IImage[]>>{
    return this.httpService.get('sectors/'+`${id}`+'/images')
    .pipe(
      map(
        (res: IServerResponse<IImage[]>) => res as IServerResponse<IImage[]>
      )
    );
  }

  updateImageOfSector(idSector: number, data: IImage): Observable<IImage>{
    return this.httpService.put('sectors/'+`${idSector}`+'/images', data)
    .pipe(
      map(
        (res: IServerResponse<IImage>) => res.data as IImage
      )
    );
  }

}
