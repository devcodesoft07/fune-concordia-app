import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IRoutes } from '../../models/routes.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RoutesService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getRoutes(): Observable<IRoutes[]> {
    return this.httpClient.get('assets/json/routes.json')
    .pipe(
      map(
        (res: IRoutes[]) => res as IRoutes[]
      )
    );
  }
}
