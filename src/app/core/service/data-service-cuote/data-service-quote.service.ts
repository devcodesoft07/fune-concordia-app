/* eslint-disable no-underscore-dangle */
/* eslint-disable @typescript-eslint/member-ordering */
import { Injectable } from '@angular/core';
import { BehaviorSubject, ReplaySubject } from 'rxjs';
import { AuthConstants } from '../../config/auth-constants.config';
import { IFuneralService } from '../../models/funeral-service/funeralService.model';
import { ISector } from '../../models/memorial-lot/sector.model';
import { IDataToQuote } from '../../models/quotes/quote.model';
import { DataBaseService } from '../data-base/data-base.service';
import { ToastService } from '../toast/toast.service';

@Injectable({
  providedIn: 'root'
})
export class DataServiceQuoteService {
  private products: IDataToQuote<ISector|IFuneralService> [] = [];

  private dataToCuote = new BehaviorSubject<any>({});
  private totalDiscount = new BehaviorSubject<any>(1);
  private objectSourceList = new BehaviorSubject<IDataToQuote<ISector|IFuneralService> []>([]);

  dataToCuote$ = this.dataToCuote.asObservable();
  totalDiscount$ = this.totalDiscount.asObservable();
  objectSourceList$ = this.objectSourceList.asObservable();

  productSector!: IDataToQuote<ISector|IFuneralService>;
  productService!: IDataToQuote<ISector|IFuneralService>;
  constructor(
    private dataBaseService: DataBaseService,
    private toasService: ToastService
  ) {
  }
  sendObjectSource(data: any){
    this.dataToCuote.next(data);
  }

  sendObjectTotalDiscount(data: number){
    this.totalDiscount.next(data);
  }

  addProduct(data: IDataToQuote<ISector|IFuneralService>): void {
    const size: number = this.products.length;
    if (size <= 4) {
      if (data.title === 'Sectores') {
        if (this._verifyExistProduct(data)) {
          this.products = [...this.products, data];
          this.toasService.presentToast('Agregado a la solución integral');
        } else {
          this.toasService.presentToast('Solo se permite productos del mismo tipo, mismas configuraciones y 2 de cada tipo', 'warning');
        }
      }
      if (data.title === 'Servicio Funerario') {
        if (this._verifyExistProduct(data)) {
          this.products = [...this.products, data];
          this.toasService.presentToast('Agregado al Carrito');
        } else {
          this.toasService.presentToast('Solo se permite productos del mismo tipo, mismas configuraciones y 2 de cada tipo', 'warning');
        }
      }
    } else {
      this.toasService.presentToast('Solo se permiten cuatro productos');
    }
    this.objectSourceList.next(this.products);
  }

  private _verifyExistProduct(data: IDataToQuote<ISector | IFuneralService>): boolean {
    let res = false;
    if (this.products.length !== 0) {
      const dataFilterByTitle = this.products.filter(
        (product) => product.title === data.title
      );
      if (dataFilterByTitle.length !== 0) {
        if (data.title === 'Sectores') {
          this.productSector = dataFilterByTitle[0];
          if (dataFilterByTitle.length < 2) {
            res = this.productSector.data.id === data.data.id? true: false;
          } else {
            res = false;
          }
        } else {
          this.productService = dataFilterByTitle[0];
          if (dataFilterByTitle.length < 2) {
            res = this.productService.data.id === data.data.id? true: false;
          } else {
            res = false;
          }
        }
      } else {
        res = true;
      }
    } else {
      res = true;
    }
    return res;
  }

  deleteProduct(index: number): void {
    this.products.splice(index, 1);
    this.objectSourceList.next(this.products);
  }

  clearProducts(): void {
    this.products = [];
    this.objectSourceList.next(this.products);
  }

}
