import { TestBed } from '@angular/core/testing';

import { DataServiceQuoteService } from './data-service-quote.service';

describe('DataServiceQuoteService', () => {
  let service: DataServiceQuoteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataServiceQuoteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
