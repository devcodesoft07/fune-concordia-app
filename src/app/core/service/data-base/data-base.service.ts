import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import * as cordovaSQLiteDriver from 'localforage-cordovasqlitedriver';
import { BehaviorSubject, from, Observable, of } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class DataBaseService {
  private storageReady = new BehaviorSubject(false);
  constructor(
    private storage: Storage
  ) {
    this.init();
  }

  async init(){
    await this.storage.defineDriver(cordovaSQLiteDriver);
    await this.storage.create();
    this.storageReady.next(true);
  }

  storeAllData(key: string, data: any[]) {
    this.storage.set(key, data);
  }

  getData(key: string) {
    return this.storageReady.pipe(
      filter( ready => ready),
      switchMap(_ => from(this.storage.get(key)) || of([]))
    );
  }

  existKey(key: string): Observable<boolean> {
    return this.getData(key).pipe(
      map(
        (res: any[]) => {
          if (res !== null) {
            if (res.length !== 0) {
              return true;
            } else {
              return false;
            }
          } else {
            return false;
          }
        }
      )
    );
  }

  async addData(data: any, key: string){
    let storedData: any[];
    await this.getData(key).subscribe(
      (res: any[]) => {
        storedData = res;
      }
    );
    storedData.push(data);
    return this.storage.set(key, storedData);
  }

  async removeData(index: number, key: string){
    let storedData: any[];
    await this.getData(key).subscribe(
      (res: any[]) => {
        storedData = res;
      }
    );
    storedData.splice(index, 1);
    return this.storage.set(key, storedData);
  }

  async update(key: string, data: any[]){
    await this.storage.remove(key);
    await this.storage.set(key, data);
  }

  async storeOneData(key: string, data: any){
    await this.storage.set(key, data);
  }
}
