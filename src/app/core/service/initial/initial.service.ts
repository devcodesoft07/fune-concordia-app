import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IInitial } from '../../models/initial.model';
import { IPaginator } from '../../models/paginator.model';
import { IServerResponse } from '../../models/server-response.model';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class InitialService {

  constructor(
    private httpService: HttpService
  ) { }

  getAllInitials(): Observable<IPaginator<IInitial[]>>{
    return this.httpService.get('initials')
    .pipe(
      map(
        (res: IPaginator<IInitial[]>) => res as IPaginator<IInitial[]>
      )
    );
  }

  addInitial( data: IInitial): Observable<IInitial>{
    return this.httpService.post('initials', data)
    .pipe(
      map(
        (res: IPaginator<IInitial>) => res.data as IInitial
      )
    );
  }

  getInitial(id: number): Observable<IInitial>{
    return this.httpService.get('initials/'+id)
    .pipe(
      map(
        (res: IServerResponse<IInitial>) => res.data as IInitial
      )
    );
  }

  updateInitial(data: IInitial): Observable<IInitial>{
    return this.httpService.put('initials',data)
    .pipe(
      map(
        (res: IServerResponse<IInitial>) => res.data as IInitial
      )
    );
  }


}
