import { Component, OnInit } from '@angular/core';
import { Contact, Contacts } from '@capacitor-community/contacts';
import { isPlatform, MenuController } from '@ionic/angular';
import { IRoutes } from './core/models/routes.model';
import { DataBaseService } from './core/service/data-base/data-base.service';
import { RoutesService } from './core/service/routes/routes.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit{
  routes: IRoutes[] = [];
  constructor(
    private routesService: RoutesService,
    private menu: MenuController,
  ) {
  }

  async ngOnInit() {
    this.getRoutes();
    if (isPlatform('android')) {
      const permission = await Contacts.getPermissions();
      if (!permission.granted) {
        return;
      }
    };
  }

  getRoutes(): void {
    this.routesService.getRoutes().subscribe(
      (res: IRoutes[]) => {
        this.routes = res;
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  closeMenu(): void {
    this.menu.close('main');
  }
}
